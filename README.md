# Folder Structure
---

- ### POC
    - #### Application Files
        - `AppDelegate.swift`
        - `SceneDelegate.swift`
        - `Main.storyboard`

    - #### Localization
        - _Localization related files_
     
    - #### Supporting Files
        - Assets
        - `LaunchScreen.storyboard`
        - <code<UI-Extensions.swift`
            ___For adding run time attributes from XCode Inspector___
    

    - #### Utilities

        - __Http__ _(for API Service)_ 
            - `HttpUtils.swift` for making URLSession request
            - `Endpoints.swift` API Urls        
        
        - __Padded-TextField__
            - `UITextFieldPadded.swift`
            - Custom UITextField for adding padding from storyboard
            
        - __Image-ScrollView__
            - `ImageScrollView.swift`
                - Custom ImageScrollView for zooming imageview
                - Taken from https://github.com/huynguyencong/ImageScrollView/blob/master/Sources/ImageScrollView.swift
                
        - __Infinite-ScrollView__
            - `IFScrollView.swift`
                - Custom UIScrollView for viewing scrolling pages circular(infinite).
                - horizontal/vertical direction
                
        - `LazyImageView.swift`
            - Custom UIImageView for loading and caching images from URL.
        
        - `DateUtils.swift`
            - For showing `time ago` and `time after` from date or UTC strings.
       
        - `SafeKeyboard.swift`
            - For resizing view based on notifications when keyboards shows/hides.
            

    - #### Scenes

        - __Authentication__
            _(Login, signup using CoreData, Biometrics-Verification)_
        
            - Model
            - View (Storyboard)
            - Login (View, ViewModel)
            - Signup (View, ViewModel)
            - Services

        - __DownloadsTab__
        _(Download add new dwonloadable file url, pause, resume, cancel, downloads. Auto pause resume downloads on view appears/disappears, change maximum concurrent downloads)_
        
            - Model
            - Services
            - View
                - `Downloads.storyboard`
                - Downloads-List
                - View-Photos
            - ViewModel
      
        - __UtilsTab__
        _(Adding new local notification with image, See Delivered Notifications, Pending Notifications)_
        
            - Model
            - Services
            - View (storyboard, view)
            - ViewModel
   
        - __PostsTab__
        _(View Posts, comments, Share Post)_
            - Model
            - Posts.storyboard
            - PostDetails (View, ViewModel)
            - PostsList (View, ViewModel)
        
        - __SettingsTab__
        _(Turn on/off notifications, Logout cuurent user)_
        
            - `Settings.storyboard`
            - `SettingsViewController.switf`
            - `SeetingsViewModel.swift`
        
    ###

- ### POCTests
    - Other_Tests
    - URLMockService
        - json-files (Mock Responses)
        (Service files for Mocking URLSession)
    - Scenes
        _(ViewModels Unit Tests)_
        - Authentication
        - DownloadsTab_Tests
        - PostsTab_Tests
        - SettingsTab
        - UtilsTab

- ### POCUITests
    - `POCUITests.swift`