//
//  PostsDetailViewModel_Tests.swift
//  POCTests
//
//  Created by Mayank Vamja on 30/04/21.
//

import XCTest
@testable import POC

class PostsDetailViewModel_Tests: XCTestCase {

    var vm: PostDetailViewModel!
    var testDelegate: TestPostDetailViewModelDelegate!
    let expectation = XCTestExpectation()

    let testPostId = "SFAt3mJK0qu4QOd9LgSX"
    let url = "\(Endpoints.posts)/SFAt3mJK0qu4QOd9LgSX/comment"
    
    override func setUpWithError() throws {
        testDelegate = TestPostDetailViewModelDelegate()
        testDelegate.asyncExpectation = expectation
        
        let config = URLSessionConfiguration.default
        config.protocolClasses = [URLMockProtocol.self]
        
        vm = PostDetailViewModel(config: config)
        vm.delegate = testDelegate
        URLMockService.clean()
    }

    override func tearDownWithError() throws {
        vm = nil
    }

    func test_fetch_post_comments() throws {
        let mock = URLMockObject(url, "post-comment")
        URLMockService.add(mock)
        
        vm.fetchComments(from: testPostId)
        wait(for: [expectation], timeout: 5)
    }

    
    func test_fetch_post_comments_failure() throws {
//        let mock = URLMockObject(url, "invalid-app-id", 403)
//        URLMockService.add(mock)
        
        vm.fetchComments(from: testPostId)
        wait(for: [expectation], timeout: 5)
    }
    
    func test_fetch_post_comments_invalid_response() throws {
        let mock = URLMockObject(url, "invalid-json", 403)
        URLMockService.add(mock)
        
        vm.fetchComments(from: testPostId)
        wait(for: [expectation], timeout: 5)
    }
}

class TestPostDetailViewModelDelegate: PostDetailViewModelDelegate {
    var asyncExpectation: XCTestExpectation!
    func loader(_ loading: Bool) {}
    
    func postDetail(didLoadComments changed: Bool) {
        asyncExpectation.fulfill()
    }
    
    func postDetail(didFailed errorMessage: String) {
        XCTAssertTrue(errorMessage.count > 0)
        asyncExpectation.fulfill()
    }
    
}
