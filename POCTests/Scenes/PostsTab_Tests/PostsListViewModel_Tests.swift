//
//  PostsListViewModel_Tests.swift
//  POCTests
//
//  Created by Mayank Vamja on 30/04/21.
//

import XCTest
@testable import POC

class PostsViewModel_Tests: XCTestCase {

    var vm: PostsViewModel!
    var testDelegate: TestPostsViewModelDelegate!
    let expectation = XCTestExpectation()

    override func setUpWithError() throws {
        testDelegate = TestPostsViewModelDelegate()
        testDelegate.asyncExpectation = expectation
        
        let config = URLSessionConfiguration.default
        config.protocolClasses = [URLMockProtocol.self]
        
        vm = PostsViewModel(config: config)
        vm.delegate = testDelegate
        URLMockService.clean()
    }

    override func tearDownWithError() throws {
        vm = nil
    }

    func test_fetch_fresh_posts_data() throws {
        let mock = URLMockObject(Endpoints.posts, "posts-response")
        URLMockService.add(mock)
        
        vm.fetchPosts(reset: true)
        wait(for: [expectation], timeout: 5)
        
        XCTAssertTrue(self.vm.posts.count > 0)
    }
    
    func test_fetch_posts_data() throws {
        let mock = URLMockObject(Endpoints.posts, "posts-response")
        URLMockService.add(mock)
        
        vm.fetchPosts()
        wait(for: [expectation], timeout: 5)
        
        XCTAssertTrue(self.vm.posts.count > 0)
    }
    
    func test_fetch_posts_with_api_error() throws {
        vm.fetchPosts()
        wait(for: [expectation], timeout: 5)
    }
    
    func test_fetch_posts_with_invalid_response() throws {
        let mock = URLMockObject(Endpoints.posts, "invalid-app-id", 403)
        URLMockService.add(mock)
        
        vm.fetchPosts()
        wait(for: [expectation], timeout: 5)
    }
    
    func test_fetch_posts_data_when_already_loading() throws {
        vm.loading = true
        vm.fetchPosts()
        XCTAssertTrue(vm.loading)
    }

}

class TestPostsViewModelDelegate: PostsViewModelDelegate {
    var asyncExpectation: XCTestExpectation!
    func loader(_ loading: Bool) {}
    
    func posts(didChange posts: [Post]) {
        asyncExpectation.fulfill()
    }
    
    func posts(didFailed errorMessage: String) {
        XCTAssert(errorMessage.count > 0)
        asyncExpectation.fulfill()
    }
    
}
