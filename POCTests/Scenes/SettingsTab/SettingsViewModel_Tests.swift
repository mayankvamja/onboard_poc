//
//  SettingsViewModel_Tests.swift
//  POCTests
//
//  Created by Mayank Vamja on 02/05/21.
//

import XCTest
@testable import POC

class SettingsViewModel_Tests: XCTestCase {
    
    var vm: SettingsViewModel!

    override func setUpWithError() throws {
        vm = SettingsViewModel()
    }

    override func tearDownWithError() throws {
        vm = nil
    }

    func test_notification_authenticity_check() throws {
        let expectation = XCTestExpectation()
        vm.setNotificationAuthorization = { _ in
            expectation.fulfill()
        }
        vm.checkNotificationAuthenticity()
        wait(for: [expectation], timeout: 2.0)
    }
}
