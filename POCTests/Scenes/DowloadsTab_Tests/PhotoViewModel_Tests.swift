//
//  PhotoViewModel_Tests.swift
//  POCTests
//
//  Created by Mayank Vamja on 02/05/21.
//

import XCTest
@testable import POC

class PhotoViewModel_Tests: XCTestCase {
    
    var vm: PhotoViewModel!
    var testDelegate: TestPhotoViewModelDelegate!
    
    let expectation = XCTestExpectation()
    
    override func setUpWithError() throws {
        testDelegate = TestPhotoViewModelDelegate()
        testDelegate.asyncExpectation = expectation
        
    }

    override func tearDownWithError() throws {
        vm = nil
    }
    
    func test_fetch_directory_images() throws {
        let fm = FileManager.default
        let documents = fm.urls(for: .documentDirectory, in: .userDomainMask).first!
        //let filePath = documents.appendingPathComponent("temp-test-file-111.png").absoluteString
        //fm.createFile(atPath: filePath, contents: nil, attributes: nil)
        
        print(documents.absoluteString)
        vm = PhotoViewModel(for: documents, start: "0-5616x3744.jpg")
        vm.delegate = testDelegate
        vm.loadPhotos()
        wait(for: [expectation], timeout: 5)
        
        //try? fm.removeItem(atPath: filePath)
    }
    
    func test_fetch_directory_images_failed() throws {
        let documents = FileManager.default.urls(for: .cachesDirectory, in: .userDomainMask).first!
        vm = PhotoViewModel(for: documents, start: "111.jpg")
        vm.delegate = testDelegate
        vm.loadPhotos()
        wait(for: [expectation], timeout: 5)
    }
    
}

class TestPhotoViewModelDelegate: PhotoViewModelDelegate {
    var asyncExpectation: XCTestExpectation!
    func images(didLoad images: [UIImage], starting index: Int) {
        asyncExpectation.fulfill()
    }
    
    func images(didFailed errorMessage: String) {
        XCTAssertTrue(errorMessage.count > 0)
        asyncExpectation.fulfill()
    }
    
}
