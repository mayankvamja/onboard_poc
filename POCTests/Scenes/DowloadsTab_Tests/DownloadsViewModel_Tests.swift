//
//  DownloadsViewModel_Tests.swift
//  POCTests
//
//  Created by Mayank Vamja on 30/04/21.
//

import XCTest
@testable import POC

class DownloadsViewModel_Tests: XCTestCase {

    var vm: DownloadsViewModel!
    var testDelegate: TestDownloadsViewModelDelegate!
    
    let expectation = XCTestExpectation()
    let testItem = DownloadFileItem(from: PicsumCDFileItem(id: "123", author: "XYZ", width: 100, height: 100, url: "https://picsum.photos/id/0/5616/3744", downloadURL: "https://picsum.photos/id/0/5616/3744"))
    
    
    override func setUpWithError() throws {
        testDelegate = TestDownloadsViewModelDelegate()
        testDelegate.asyncExpectation = expectation
        vm = DownloadsViewModel()
        vm.delegate = testDelegate
    }

    override func tearDownWithError() throws {
        vm = nil
    }
    
    func test_fetch_download_items() throws {
        vm.fetchPhotos()
        wait(for: [expectation], timeout: 20.0)
        XCTAssertTrue(self.vm.data.count > 0)
    }
    
    func test_fetch_from_server() throws {
        vm.fetchPhotosFromServer()
        wait(for: [expectation], timeout: 20.0)
        
        XCTAssertTrue(self.vm.data.count > 0)
    }
    
    func test_handle_api_error() throws {
        vm.handleError(error: NSError(domain: "", code: -1, userInfo: nil))
        wait(for: [expectation], timeout: 1.0)
    }
    
    func test_startDownload() throws {
        testItem.download = nil
        
        vm.startDownload(for: testItem)
        wait(for: [expectation], timeout: 2.0)
        
        let status = testItem.download!.status
        XCTAssertTrue(status == .Downloading || status == .Waiting)
    }
    
    func test_pauseDownload() throws {
        testItem.download = FileDownloadModel()
        testItem.download!.status = .Downloading
        
        vm.pauseDownload(for: testItem)
        wait(for: [expectation], timeout: 2.0)
        
        XCTAssertTrue(testItem.download!.status == .Paused)
    }
    
    func test_resumeDownload() throws {
        testItem.download = FileDownloadModel()
        testItem.download!.status = .Paused
        
        vm.resumeDownload(for: testItem)
        wait(for: [expectation], timeout: 2.0)
        
        let status = testItem.download!.status
        XCTAssertTrue(status == .Downloading || status == .Waiting)
    }
    
    func test_cancelDownload() throws {
        testItem.download = FileDownloadModel()
        testItem.download!.status = .Downloading
        
        vm.cancelDownload(for: testItem)
        wait(for: [expectation], timeout: 2.0)
        
        XCTAssertNil(testItem.download)
    }
    
    func test_startAllDownload() throws {
        vm.data = [testItem]
        testItem.download = nil
        
        vm.startDownloadAll()
        wait(for: [expectation], timeout: 2.0)
        
        let status = testItem.download!.status
        XCTAssertTrue(status == .Downloading || status == .Waiting)
    }
    
    func test_pauseAllDownload() throws {
        vm.data = [testItem]
        testItem.download = FileDownloadModel()
        testItem.download!.status = .Downloading
        
        vm.pauseDonwloadAll()
        wait(for: [expectation], timeout: 2.0)
        
        XCTAssertTrue(testItem.download!.status == .Paused)
    }
    
    func test_resumeAllDownload() throws {
        vm.data = [testItem]
        testItem.download = FileDownloadModel()
        testItem.download!.status = .Paused
        
        vm.resumeDownloadAll()
        wait(for: [expectation], timeout: 2.0)
        
        let status = testItem.download!.status
        XCTAssertTrue(status == .Downloading || status == .Waiting)
    }
    
    
    func test_cancelAllDownload() throws {
        vm.data = [testItem]
        testItem.download = FileDownloadModel()
        testItem.download!.status = .Downloading
        
        vm.cancelDownloadAll()
        wait(for: [expectation], timeout: 2.0)
        
        XCTAssertNil(testItem.download)
    }
    
    func test_autoPause_off() throws {
        var settings = DownloadSettingsModel()
        settings.autoPause = false
        XCTAssertFalse(vm.settings.autoPause)
    }
    
    func test_autoPause_on() throws {
        var settings = DownloadSettingsModel()
        settings.autoPause = true
        
        vm.data = [testItem]
        
        vm.startDownloadAll()
        XCTAssertTrue(vm.settings.autoPause)
        vm.viewDidDissppear()
        
        XCTAssertNil(vm.data.first(where: { (item) -> Bool in
            item.download?.status == .Downloading
        }) )
        
        vm.viewDidAppear()
        
        XCTAssertNotNil(vm.data.first(where: { (item) -> Bool in
            item.download?.status == .Downloading
        }) )
    }
    
    func test_filter_list() throws {
        vm.data = [testItem]
        XCTAssertEqual(vm.filterList(0).count, 1)
        XCTAssertEqual(vm.filterList(1).count, 0)
        XCTAssertEqual(vm.filterList(2).count, 0)
        XCTAssertEqual(vm.filterList(3).count, 0)
        
        testItem.download = FileDownloadModel()
        testItem.download!.status = .Downloading
        XCTAssertEqual(vm.filterList(0).count, 1)
        XCTAssertEqual(vm.filterList(1).count, 1)
        XCTAssertEqual(vm.filterList(2).count, 0)
        XCTAssertEqual(vm.filterList(3).count, 0)

        testItem.download!.status = .Paused
        XCTAssertEqual(vm.filterList(0).count, 1)
        XCTAssertEqual(vm.filterList(1).count, 0)
        XCTAssertEqual(vm.filterList(2).count, 1)
        XCTAssertEqual(vm.filterList(3).count, 0)

        testItem.download = nil
        testItem.downloadedFilePath = "file-path/file.ext"
        XCTAssertEqual(vm.filterList(0).count, 1)
        XCTAssertEqual(vm.filterList(1).count, 0)
        XCTAssertEqual(vm.filterList(2).count, 0)
        XCTAssertEqual(vm.filterList(3).count, 1)
    }
    
    func test_check_new_item() throws {
        vm.data = []
        vm.check(newItemWith: "", urlString: "")
        XCTAssertEqual(vm.data.count, 0)
        
        vm.check(newItemWith: "asjnd", urlString: "")
        XCTAssertEqual(vm.data.count, 0)
        
        vm.check(newItemWith: "dasjdk", urlString: "asdb")
        XCTAssertEqual(vm.data.count, 0)
        
        vm.check(newItemWith: "asasd", urlString: "https://example.com")
        XCTAssertEqual(vm.data.count, 1)
    }
    
    func test_removefile() throws {
        testItem.downloadedFilePath = "not-nil"
        vm.removeFile(testItem, at: 0)
        XCTAssertNil(testItem.downloadedFilePath)
    }

}

class TestDownloadsViewModelDelegate: DownloadsViewModelDelegate {
    var asyncExpectation: XCTestExpectation!
    func loader(_ loading: Bool) {}
    
    func didFilesLoad() {
        asyncExpectation.fulfill()
    }
    
    func didFilesFailed(errorMessage: String) {
        XCTAssertTrue(errorMessage.count > 0)
        asyncExpectation.fulfill()
    }
    
    func updateRow(for fileId: String) {
        asyncExpectation.fulfill()
    }
    
    func updateRow(for fileId: String, with data: RowDownloadData) {
        asyncExpectation.fulfill()
    }
}
