//
//  LoginViewModel_Tests.swift
//  POCTests
//
//  Created by Mayank Vamja on 02/05/21.
//

import XCTest
@testable import POC

class LoginViewModel_Tests: XCTestCase {
    
    var vm: LoginViewModel!

    override func setUpWithError() throws {
        vm = LoginViewModel()
    }

    override func tearDownWithError() throws {
        vm = nil
    }

    func test_email_nil() throws {
        let expectedError = ValidationError.emailRequired.localizedDescription
        vm.checkForEmail(text: nil)
        XCTAssertEqual(vm.emailError, expectedError)
    }
    
    func test_email_empty() throws {
        let expectedError = ValidationError.emailRequired.localizedDescription
        vm.checkForEmail(text: "")
        XCTAssertEqual(vm.emailError, expectedError)
    }
    
    func test_email_invalid() throws {
        let expectedError = ValidationError.invalidEmail.localizedDescription
        vm.checkForEmail(text: "invalidemail@email")
        XCTAssertEqual(vm.emailError, expectedError)
    }
    
    func test_email_valid() throws {
        vm.checkForEmail(text: "valid123@email.com")
        XCTAssertNil(vm.emailError)
    }
    
    func test_password_nil() throws {
        let expectedError = ValidationError.passwordRequired.localizedDescription
        vm.checkForPassword(text: nil)
        XCTAssertEqual(vm.passwordError, expectedError)
    }
    
    func test_password_empty() throws {
        let expectedError = ValidationError.passwordRequired.localizedDescription
        vm.checkForPassword(text: "")
        XCTAssertEqual(vm.passwordError, expectedError)
    }
    
    func test_password_short() throws {
        let expectedError = ValidationError.shortPassword.localizedDescription
        vm.checkForPassword(text: "nil")
        XCTAssertEqual(vm.passwordError, expectedError)
    }
    
    /*func test_password_invalid() throws {
        let expectedError = ValidationError.invalidPassword.localizedDescription
        vm.checkForPassword(text: "nilasdasdasd")
        XCTAssertEqual(vm.passwordError, expectedError)
    }*/
    
    func test_password_valid() throws {
        vm.checkForPassword(text: "Test@123")
        XCTAssertNil(vm.passwordError)
    }
    
    func test_login_button_enable_disable() throws {
        vm.checkForEmail(text: nil)
        vm.checkForLoginButton()
        XCTAssertFalse(vm.canTapLogin)
        
        vm.checkForEmail(text: "valid@email.com")
        vm.checkForPassword(text: nil)
        XCTAssertFalse(vm.canTapLogin)
        
        vm.checkForPassword(text: "Test@123")
        XCTAssertTrue(vm.canTapLogin)
    }
    
    func test_login() throws {
        vm.email = "test@test.com"
        vm.password = "Test@123"
        
        let tempUser = AuthUser(context: AuthDataStore.shared.context)
        tempUser.email = "test@test.com"
        tempUser.dateOfBirth = Date()
        tempUser.firstName = "test"
        tempUser.lastName = "test"
        tempUser.id = "testID123"
        tempUser.password = "Test@123"
        
        var expectation = XCTestExpectation()
        AuthDataStore.shared.save { (_) in expectation.fulfill() }
        wait(for: [expectation], timeout: 2)

        /// Test for Invalid credentials
        vm.email = "test@st.com"
        vm.password = "Tast@123"
        
        expectation = XCTestExpectation()
        vm.onLoginResult = { (success, _) in
            XCTAssertFalse(success)
            expectation.fulfill()
        }
        vm.login()
        wait(for: [expectation], timeout: 2)
        
        /// Test for valid credentials
        vm.email = "test@test.com"
        vm.password = "Test@123"
        
        expectation = XCTestExpectation()
        vm.onLoginResult = { (success, _) in
            XCTAssertTrue(success)
            expectation.fulfill()
        }
        vm.login()
        wait(for: [expectation], timeout: 2)

        AuthDataStore.shared.context.delete(tempUser)
        expectation = XCTestExpectation()
        AuthDataStore.shared.save { (_) in expectation.fulfill() }
        wait(for: [expectation], timeout: 2)
    }

}
