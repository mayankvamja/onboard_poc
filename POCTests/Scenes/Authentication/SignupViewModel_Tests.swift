//
//  SignupViewModel_Tests.swift
//  POCTests
//
//  Created by Mayank Vamja on 02/05/21.
//

import XCTest
@testable import POC

class SignupViewModel_Tests: XCTestCase {
    
    var vm: SignupViewModel!

    override func setUpWithError() throws {
        vm = SignupViewModel()
    }

    override func tearDownWithError() throws {
        vm = nil
    }

    func test_first_name() throws {
        var expectedError = ValidationError.requiredFirstName.localizedDescription
        vm.checkForFirstName(text: nil)
        XCTAssertEqual(vm.firstNameError, expectedError)
        
        vm.checkForFirstName(text: "")
        XCTAssertEqual(vm.firstNameError, expectedError)
        
        expectedError = ValidationError.invalidFirstName.localizedDescription
        vm.checkForFirstName(text: "asdnaksndknasdsabjdbsajdbfjlsbadlfbabsdldjlsbf")
        XCTAssertEqual(vm.firstNameError, expectedError)
        
        vm.checkForFirstName(text: "First")
        XCTAssertNil(vm.firstNameError)
    }
    
    func test_last_name() throws {
        var expectedError = ValidationError.requiredLastName.localizedDescription
        vm.checkForLastName(text: nil)
        XCTAssertEqual(vm.lastNameError, expectedError)
        
        vm.checkForLastName(text: "")
        XCTAssertEqual(vm.lastNameError, expectedError)
        
        expectedError = ValidationError.invalidLastName.localizedDescription
        vm.checkForLastName(text: "asdnaksndknasdsabjdbsajdbfjlsbadlfbabsdldjlsbf")
        XCTAssertEqual(vm.lastNameError, expectedError)
        
        vm.checkForLastName(text: "Last")
        XCTAssertNil(vm.lastNameError)
    }
    
    func test_mobile_number() throws {
        let expectedError = ValidationError.invalidPhoneNumber.localizedDescription
        vm.checkForPhoneNumber(text: "asdn123124213")
        XCTAssertEqual(vm.phoneNumberError, expectedError)
        
        vm.checkForPhoneNumber(text: "+1 123")
        XCTAssertEqual(vm.phoneNumberError, expectedError)

        vm.checkForPhoneNumber(text: "")
        XCTAssertNil(vm.phoneNumberError)
        
        vm.checkForPhoneNumber(text: "+91 9123123123")
        XCTAssertNil(vm.phoneNumberError)
    }
    
    func test_birth_date() throws {
        let date = Date()
        vm.checkForBirthDate(date: date)
        XCTAssertEqual(vm.birthDate, date)
    }
    
    func test_email() throws {
        var expectedError = ValidationError.emailRequired.localizedDescription
        vm.checkForEmail(text: nil)
        XCTAssertEqual(vm.emailError, expectedError)
        
        expectedError = ValidationError.emailRequired.localizedDescription
        vm.checkForEmail(text: "")
        XCTAssertEqual(vm.emailError, expectedError)
        
        expectedError = ValidationError.invalidEmail.localizedDescription
        vm.checkForEmail(text: "invalidemail@email")
        XCTAssertEqual(vm.emailError, expectedError)
        
        vm.checkForEmail(text: "valid123@email.com")
        XCTAssertNil(vm.emailError)
    }
    
    func test_password() throws {
        var expectedError = ValidationError.passwordRequired.localizedDescription
        vm.checkForPassword(text: nil)
        XCTAssertEqual(vm.passwordError, expectedError)
        
        expectedError = ValidationError.passwordRequired.localizedDescription
        vm.checkForPassword(text: "")
        XCTAssertEqual(vm.passwordError, expectedError)
        
        expectedError = ValidationError.shortPassword.localizedDescription
        vm.checkForPassword(text: "nil")
        XCTAssertEqual(vm.passwordError, expectedError)
        
        //expectedError = ValidationError.invalidPassword.localizedDescription
        //vm.checkForPassword(text: "nilasdasdasd")
        //XCTAssertEqual(vm.passwordError, expectedError)
        
        vm.checkForPassword(text: "Test@123")
        XCTAssertNil(vm.passwordError)
    }
    
    func test_confirm_password() throws {
        var expectedError = ValidationError.confirmPasswordRequired.localizedDescription
        vm.checkForConfirmPassword(text: nil)
        XCTAssertEqual(vm.confirmPasswordError, expectedError)
        
        vm.password = "Test@123"
        expectedError = ValidationError.mismatchPaassword.localizedDescription
        vm.checkForConfirmPassword(text: "Check@12")
        XCTAssertEqual(vm.confirmPasswordError, expectedError)
        
        vm.checkForConfirmPassword(text: "Test@123")
        XCTAssertNil(vm.emailError)
    }
    
    func test_signup_button_enable_disable() throws {
        vm.checkForSignup()
        XCTAssertFalse(vm.canSignup)
        
        vm.firstName = "F"
        vm.lastName = "L"
        vm.email = "email@email.com"
        vm.password = "Test@123"
        vm.confirmPassword = "Test@123"
        vm.birthDate = Date()
        
        vm.checkForSignup()
        XCTAssertTrue(vm.canSignup)
    }
    
    func test_signup() throws {
        vm.email = "test@test.com"
        vm.birthDate = Date()
        vm.firstName = "test"
        vm.lastName = "test"
        vm.password = "Test@123"
        
        var expectation = XCTestExpectation()
        vm.onSignupResult = { (success, _) in
            XCTAssertTrue(success)
            expectation.fulfill()
        }
        vm.registerUser()
        wait(for: [expectation], timeout: 2)
        
        expectation = XCTestExpectation()
        vm.onSignupResult = { (success, _) in
            XCTAssertFalse(success)
            expectation.fulfill()
        }
        vm.registerUser()
        wait(for: [expectation], timeout: 2)
        
        expectation = XCTestExpectation()
        AuthDataStore.shared.deleteUser(by: vm.email, completion: { (_) in
            expectation.fulfill()
        })
        wait(for: [expectation], timeout: 5)
    }
    
    func test_for_registered_email() throws {
        vm.email = "test@test.com"
        vm.birthDate = Date()
        vm.firstName = "test"
        vm.lastName = "test"
        vm.password = "Test@123"
        
        var expectation = XCTestExpectation()
        vm.onSignupResult = { (success, _) in
            XCTAssertTrue(success)
            expectation.fulfill()
        }
        vm.registerUser()
        wait(for: [expectation], timeout: 2)
        
        let expectedError = ValidationError.emailRegistered.localizedDescription
        vm.checkForEmail(text: vm.email)
        XCTAssertEqual(vm.emailError, expectedError)
        
        expectation = XCTestExpectation()
        AuthDataStore.shared.deleteUser(by: vm.email, completion: { (_) in
            expectation.fulfill()
        })
        wait(for: [expectation], timeout: 5)
    }
    
}
