//
//  UtilsViewModel_Tests.swift
//  POCTests
//
//  Created by Mayank Vamja on 02/05/21.
//

import XCTest
@testable import POC

class UtilsViewModel_Tests: XCTestCase {
    
    var vm: UtilsViewModel!

    override func setUpWithError() throws {
        vm = UtilsViewModel()
        vm.checkAuthorization()
    }

    override func tearDownWithError() throws {
        vm = nil
    }

    func test_add_new_notification_with_failure() throws {
        let expectation = XCTestExpectation()
        vm.onNotificationFailed = { msg in
            XCTAssertEqual("Please provide title or body.".localized, msg)
            expectation.fulfill()
        }
        vm.addNewNotification(title: "", body: "", image: nil, datetime: Date(timeIntervalSinceNow: 100))
        wait(for: [expectation], timeout: 2)
    }
    
    func test_add_new_notification() throws {
        var expectation = XCTestExpectation()
        //vm.onNotificationAdded = { expectation.fulfill() }
        vm.addNewNotification(title: "NOTIFICCATION", body: "-", image: nil, datetime: Date())
        
        var originalCount: Int!
        var request: UNNotificationRequest!
        vm.onUpdateScheduledNotificationsCount = { [self] count in
            originalCount = count
            request = vm.pendingNotifications!.first!
            expectation.fulfill()
        }
        wait(for: [expectation], timeout: 2)
        
        // Checking for clearNotification(withIdentifier:)
        
        expectation = XCTestExpectation()
        vm.onUpdateScheduledNotificationsCount = {
            XCTAssertEqual($0, originalCount - 1)
            expectation.fulfill()
        }
        
        vm.cancelNotification(with: request.identifier)
        wait(for: [expectation], timeout: 2)
    }
    
    func test_add_new_notification_with_image() throws {
        let expectation = XCTestExpectation()
        vm.onNotificationAdded = { expectation.fulfill() }
        vm.addNewNotification(title: "NOTIFICCATION", body: "-", image: "https://i.picsum.photos/id/336/1/1.jpg?hmac=0IS3crbdMwpmGbmtsQxsHNHLPKGlgCECybSBAA0Fbws", datetime: Date())
        wait(for: [expectation], timeout: 5)
    }
    
    func test_add_new_notification_with_image_failure() throws {
        let expectation = XCTestExpectation()
        vm.onNotificationFailed = { message in
            XCTAssertTrue(message.count > 0)
            expectation.fulfill()
        }
        vm.onNotificationAdded = { expectation.fulfill() }
        vm.addNewNotification(title: "NOTIFICCATION", body: "-", image: "https://i.picsum.photos/123", datetime: Date())
        wait(for: [expectation], timeout: 5)
    }
    
    func test_notification_history() throws {
        let expectation = XCTestExpectation()
        vm.onUpdateDeliveredNotificationsCount = { _ in expectation.fulfill() }
        vm.fetchDeliveredNotifications()
        wait(for: [expectation], timeout: 2)
    }
    
    func test_scheduled_notificactions() throws {
        let expectation = XCTestExpectation()
        vm.onUpdateScheduledNotificationsCount = { _ in expectation.fulfill() }
        vm.fetchScheduledNotifications()
        wait(for: [expectation], timeout: 2)
    }
    
    func test_clear_notification_withId() throws {
        var expectation = XCTestExpectation()
        var originalCount: Int!
        vm.onUpdateDeliveredNotificationsCount = {
            originalCount = $0
            expectation.fulfill()
        }
        vm.fetchDeliveredNotifications()
        wait(for: [expectation], timeout: 2)
        
        expectation = XCTestExpectation()
        vm.onUpdateDeliveredNotificationsCount = {
            XCTAssertEqual($0, originalCount)
            expectation.fulfill()
        }
        vm.clearNotification(with: "identifier")
        wait(for: [expectation], timeout: 2)
    }

    func test_clear_all_notificactions() throws {
        let expectation = XCTestExpectation()
        vm.onUpdateDeliveredNotificationsCount = {
            XCTAssertEqual($0, 0)
            expectation.fulfill()
        }
        vm.clearNotificationCenter()
        wait(for: [expectation], timeout: 2)
    }
    
    func test_cancel_all_notificactions() throws {
        let expectation = XCTestExpectation()
        vm.onUpdateScheduledNotificationsCount = {
            XCTAssertEqual($0, 0)
            expectation.fulfill()
        }
        vm.cancelAllNotifications()
        wait(for: [expectation], timeout: 2)
    }
}
