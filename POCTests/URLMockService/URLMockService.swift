//
//  MockUrlProtocol.swift
//  POCTests
//
//  Created by Mayank Vamja on 30/04/21.
//

import Foundation

struct URLMockService {
    
    static var mockObjects: [URLMockObject] = []
    
    static func add(_ object: URLMockObject) {
        mockObjects.append(object)
    }
    
    static func find(_ str: String) -> URLMockObject? {
        return mockObjects.first(where: { $0.urlString == str })
    }
    
    static func clean() {
        mockObjects.removeAll()
    }
    
}

struct URLMockObject {
    let request: URLRequest
    let response: String
    let httpResponseCode: Int
    
    init(_ urlString: String, _ responseFilePath: String, _ code: Int = 200) {
        self.request = URLRequest(url: URL(string: urlString)!)
        self.response = responseFilePath
        self.httpResponseCode = code
    }
    
    init(_ url: URL, _ responseFilePath: String, _ code: Int = 200) {
        self.request = URLRequest(url: url)
        self.response = responseFilePath
        self.httpResponseCode = code
    }
    
    var urlString: String { request.url?.absoluteString ?? "" }
}

class URLMockProtocol: URLProtocol {
    override class func canInit(with request: URLRequest) -> Bool {
        return true
    }
    
    override class func canonicalRequest(for request: URLRequest) -> URLRequest {
        return request
    }
    
    override func startLoading() {
                
        guard let url = self.request.url?.absoluteString.split(separator: "?").first else {
            self.client?.urlProtocol(self, didFailWithError: URLError(.badURL))
            return
        }

        guard let mock = URLMockService.find(String(url)) else {
            self.client?.urlProtocol(self, didFailWithError: URLError(.cannotParseResponse))
            return
        }
        
        let responseData = Data(json: mock.response)
        let httpResponse = HTTPURLResponse(url: request.url!, statusCode: mock.httpResponseCode, httpVersion: nil, headerFields: nil)!

        client?.urlProtocol(self, didReceive: httpResponse, cacheStoragePolicy: .allowed)
        client?.urlProtocol(self, didLoad: responseData)
        client?.urlProtocolDidFinishLoading(self)
    }
    
    override func stopLoading() { }
}
