//
//  Data+Extention.swift
//  POCTests
//
//  Created by Mayank Vamja on 30/04/21.
//

import Foundation

let testBundle = Bundle(identifier: "com.example.POCTests")

extension Data {
    init(reading input: InputStream) {
        self.init()
        input.open()
        let buffer = UnsafeMutablePointer<UInt8>.allocate(capacity: 1024)

        while input.hasBytesAvailable {
            let read = input.read(buffer, maxLength: 1024)
            self.append(buffer, count: read)
        }

        defer {
            input.close()
            buffer.deallocate()
        }
    }

    init(json fileName: String, bundle: Bundle? = testBundle) {
        self.init()
        
        if let path = bundle?.path(forResource: fileName, ofType: "json"), let data =  try? Data(contentsOf: URL(fileURLWithPath: path)){

            guard var str = String(data: data, encoding: .utf8) else {
                self.append(data)
                return
            }
            str.removeAll(where: { c in c == " " })
            str.removeAll(where: { c in c == "\n" })
            self.append(str.data(using: .utf8) ?? data)
        }

    }
}

struct DataUtils {
    static func getData(fromStream input: InputStream) -> Data {
        var data = Data()
        input.open()
        let buffer = UnsafeMutablePointer<UInt8>.allocate(capacity: 1024)

        defer {
            input.close()
            buffer.deallocate()
        }
        
        while input.hasBytesAvailable {
            let read = input.read(buffer, maxLength: 1024)
            data.append(buffer, count: read)
        }
        
        return data
    }
    
    static func getData(fromJson fileName: String, bundle: Bundle? = testBundle) -> Data {
        var data = Data()
        
        if let path = bundle?.path(forResource: fileName, ofType: "json"),
           let fileData =  try? Data(contentsOf: URL(fileURLWithPath: path)){
            data.append(fileData)
        }
        return data
    }
    
    static func getDictionary(fromStream input: InputStream) -> [String: Any] {
        if let dictionary = try? JSONSerialization.jsonObject(
            with: getData(fromStream: input)
        ) as? [String: Any] {
            return dictionary
        }
        return [:]
    }
    
    static func getDictionary(fromJson fileName: String, bundle: Bundle? = testBundle) -> [String: Any] {

        if let dictionary = try? JSONSerialization.jsonObject(
            with: getData(fromJson: fileName, bundle: bundle)
        ) as? [String: Any] {
            return dictionary
        }
        return [:]
    }
}
