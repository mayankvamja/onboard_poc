//
//  ValidationService_Tests.swift
//  POCTests
//
//  Created by Mayank Vamja on 02/05/21.
//

import XCTest
@testable import POC

class ValidationService_Tests: XCTestCase {

    override func setUpWithError() throws {}
    override func tearDownWithError() throws {}

    func test_password_strength_color() throws {
        var ps : PasswordStrength = .tooWeak
        XCTAssertEqual(ps.color, .red)
        
        ps = .weak
        XCTAssertEqual(ps.color, .orange)
        
        ps = .strong
        XCTAssertEqual(ps.color, .systemTeal)
        
        ps = .tooStrong
        XCTAssertEqual(ps.color, .systemGreen)
    }
    
    func test_password_strength_label() throws {
        var ps : PasswordStrength = .tooWeak
        XCTAssertEqual(ps.label, (PasswordStrength.tooWeak.rawValue).localized)
        
        ps = .weak
        XCTAssertEqual(ps.label, (PasswordStrength.weak.rawValue).localized)

        ps = .strong
        XCTAssertEqual(ps.label, (PasswordStrength.strong.rawValue).localized)

        ps = .tooStrong
        XCTAssertEqual(ps.label, (PasswordStrength.tooStrong.rawValue).localized)
    }
    
    func test_validation_error_description() throws {
        for ve in ValidationError.allCases {
            XCTAssertEqual(ve.errorDescription, ve.localizedDescription)
        }
    }
}
