//
//  String+Localization.swift
//  POC
//
//  Created by Mayank Vamja on 29/04/21.
//

import Foundation

extension String {
  var localized: String {
    return NSLocalizedString(self, comment: "\(self)_comment")
  }
}
