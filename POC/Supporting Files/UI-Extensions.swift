//
//  Storyboard-Extensions.swift
//  POC
//
//  Created by Mayank Vamja on 22/04/21.
//

import UIKit

extension CGColor {
    var UIColor: UIKit.UIColor {
        return UIKit.UIColor(cgColor: self)
    }
}

extension UIView {
    @IBInspectable var borderWidth: CGFloat {
        set { layer.borderWidth = newValue }
        get { layer.borderWidth }
    }
    
    @IBInspectable var borderColor: UIColor? {
        set { layer.borderColor = newValue?.cgColor }
        get { layer.borderColor?.UIColor }
    }
    
    @IBInspectable var cornerRadius: CGFloat {
        set { layer.cornerRadius = newValue }
        get { layer.cornerRadius }
    }
    
    @IBInspectable var masksToBounds: Bool {
        set { layer.masksToBounds = newValue }
        get { layer.masksToBounds }
    }
    
    @IBInspectable var borderRounded: Bool {
        set { layer.cornerRadius = newValue ? self.frame.height / 2 : self.layer.cornerRadius }
        get { layer.cornerRadius == self.frame.height / 2 }
    }
    
    
    // MARK: Shadow Properties
    
    @IBInspectable var shadowRadius: CGFloat {
        set { layer.shadowRadius = newValue }
        get { layer.shadowRadius }
    }
    
    @IBInspectable var shadow: Bool {
        set {
            if newValue {
                updateShadow()
            } else {
                layer.shadowColor = UIColor.clear.cgColor
                layer.shadowOpacity = 0.0
                layer.shadowRadius = 0
                
                layer.shadowPath = nil
                layer.shouldRasterize = false
            }
        }
        get {
            layer.shadowOpacity > 0.0 && layer.shadowRadius > 0
        }
    }
    
    func updateShadow() {
        
        layer.masksToBounds = false
//        layer.shadowColor = UIColor(red: 0.5, green: 0.5, blue: 0.5, alpha: 0.5).cgColor
        layer.shadowColor = UIColor.secondarySystemFill.withAlphaComponent(0.3).cgColor
        layer.cornerRadius = cornerRadius
        layer.shadowOffset = .zero
        layer.shadowOpacity = 1
        layer.shadowRadius = shadowRadius
        
        layer.shouldRasterize = true
        layer.rasterizationScale = UIScreen.main.scale
        
    }
}

extension UIViewController {
    func showMessage(title: String? = nil, message: String) {
        DispatchQueue.main.async { [weak self] in
            let alert = UIAlertController(title: title, message: message, preferredStyle: .actionSheet)
            alert.addAction(UIAlertAction(title: "Ok", style: .cancel, handler: nil))
            self?.present(alert, animated: true, completion: nil)
        }
    }
}
