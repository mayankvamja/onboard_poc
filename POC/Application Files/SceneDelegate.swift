//
//  SceneDelegate.swift
//  POC
//
//  Created by Mayank Vamja on 09/04/21.
//

import UIKit

class SceneDelegate: UIResponder, UIWindowSceneDelegate {

    var window: UIWindow?

    func scene(_ scene: UIScene, willConnectTo session: UISceneSession, options connectionOptions: UIScene.ConnectionOptions) {

        guard let _ = (scene as? UIWindowScene) else { return }

        let auth = UIStoryboard(name: "Authentication", bundle: nil)

        if AuthDataStore.shared.loggedInUser() != nil {
        //if UserDefaults.standard.bool(forKey: StorageKeys.IsLoggedIn) {
            //let main = UIStoryboard(name: "Main", bundle: nil)
            //let mainVC = main.instantiateInitialViewController()!
            let mainVC = auth.instantiateViewController(withIdentifier: "VerificationViewController")
            window?.rootViewController = mainVC
        } else {
            let mainVC = auth.instantiateInitialViewController()!
            window?.rootViewController = mainVC
        }
        
    }

    /*func sceneDidDisconnect(_ scene: UIScene) {}
    func sceneDidBecomeActive(_ scene: UIScene) {}
    func sceneWillResignActive(_ scene: UIScene) {}
    func sceneWillEnterForeground(_ scene: UIScene) {}
    func sceneDidEnterBackground(_ scene: UIScene) {}*/

}

