//
//  AppDelegate.swift
//  POC
//
//  Created by Mayank Vamja on 09/04/21.
//

import UIKit
import CoreData

@main
class AppDelegate: UIResponder, UIApplicationDelegate {

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        return true
    }

    // MARK: UISceneSession Lifecycle

    func application(_ application: UIApplication, configurationForConnecting connectingSceneSession: UISceneSession, options: UIScene.ConnectionOptions) -> UISceneConfiguration {
        
        return UISceneConfiguration(name: "Default Configuration", sessionRole: connectingSceneSession.role)
        
        /*return UserDefaults.standard.bool(forKey: "IS_LOGGED_IN") ?
            UISceneConfiguration(name: "Default Configuration", sessionRole: connectingSceneSession.role)
            : UISceneConfiguration(name: "Authentication", sessionRole: connectingSceneSession.role)*/
        
    }

    //func application(_ application: UIApplication, didDiscardSceneSessions sceneSessions: Set<UISceneSession>) {}

}

