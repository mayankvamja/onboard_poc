//
//  PostsCell.swift
//  POC
//
//  Created by Mayank Vamja on 12/04/21.
//

import UIKit

protocol PostsCellDelegate: class {
    func didTapLike(post: Post)
    func didTapCommment(post: Post)
    func didTapShare(post: Post, image: UIImage?)
}

class PostsCell: UITableViewCell {

    @IBOutlet var postOwnerImage: LazyImageView!
    @IBOutlet var postOwnerName: UILabel!
    @IBOutlet var postOwnerSubitle: UILabel!
    @IBOutlet var postBody: UILabel!
    @IBOutlet var postImage: LazyImageView!
    @IBOutlet var postLikeButton: UIButton!
    @IBOutlet var postCommentsButton: UIButton!
    @IBOutlet var postShareButton: UIButton!
    @IBOutlet var dateTimeLabel: UILabel!
    @IBOutlet var postImageHeightAnchor: NSLayoutConstraint!
    
    var post: Post!
    weak var delegate: PostsCellDelegate?
    
    var isSetHeightConstraint = false
    
    func config(with post: Post, completion: (() -> Void)? = nil) {
        self.post = post
        
        postBody.text = post.text
        postOwnerName.text = "\(post.owner.firstName) \(post.owner.lastName)"
        postOwnerSubitle.text = post.owner.email
        postLikeButton.setTitle("\(post.likes) " + "likes".localized, for: .normal)
        dateTimeLabel.text = DateUtils.timeAgo(from: post.publishDate)
        
        postOwnerImage.loadImage(from: post.owner.picture, with: UIImage(named: "avatar"))

        postImage.loadImage(from: post.image, with: UIImage(named: "placeholder"))

    }
    
    @IBAction func likeButtonTapped(_ sender: UIButton) {
        if sender.image(for: .normal) == UIImage(systemName: "heart.fill") {
            sender.setImage(UIImage(systemName: "heart"), for: .normal)
            sender.setTitle("\(post.likes) " + "likes".localized, for: .normal)
        } else {
            sender.setImage(UIImage(systemName: "heart.fill"), for: .normal)
            sender.setTitle("\(post.likes + 1) " + "likes".localized, for: .normal)
        }
        
        delegate?.didTapLike(post: post)
    }
    
    @IBAction func commentButtonTapped(_ sender: UIButton) {
        delegate?.didTapCommment(post: post)
    }
    
    @IBAction func shareButtonTapped(_ sender: UIButton) {
        delegate?.didTapShare(post: post, image: self.postImage.image)
    }
    
}
