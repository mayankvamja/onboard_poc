//
//  PostsTableViewController.swift
//  POC
//
//  Created by Mayank Vamja on 11/04/21.
//

import UIKit

class PostsTableViewController: UITableViewController {
    
    @IBOutlet var postsRefreshControl: UIRefreshControl!
    
    private var viewModel: PostsViewModel!
    var selectedPost: Post?
    
    lazy var activityIndicator: UIActivityIndicatorView = {
        let loader = UIActivityIndicatorView(style: .large)
        loader.frame = CGRect(x: CGFloat(0), y: CGFloat(0), width: tableView.bounds.width, height: CGFloat(80))
        loader.color = UIColor(named: "AppColor")
        loader.startAnimating()
        return loader
    }()

    override func viewDidLoad() {
        super.viewDidLoad()
        
        viewModel = PostsViewModel()
        viewModel.delegate = self
        viewModel.fetchPosts(reset: true)

        tableView.allowsSelection = false
        tableView.rowHeight = UITableView.automaticDimension
        tableView.tableFooterView = activityIndicator
    }
    
    @IBAction func onPostsTableRefresh(_ sender: UIRefreshControl) {
        viewModel.fetchPosts(reset: true)
    }
    
    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return viewModel.posts.count
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "posts_list_cell", for: indexPath) as! PostsCell
        
        cell.config(with: viewModel.posts[indexPath.row])
        cell.delegate = self
    
        return cell
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        UITableView.automaticDimension
    }
    
    override func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        UITableView.automaticDimension
    }

    override func scrollViewDidScroll(_ scrollView: UIScrollView) {
        
        let offset = scrollView.contentOffset
        let bounds = scrollView.bounds
        let size = scrollView.contentSize
        let inset = scrollView.contentInset
        
        let y = offset.y + bounds.size.height - inset.bottom
        let h = size.height
        let reload_distance:CGFloat = 300.0
        
        if (y > (h - reload_distance) && !viewModel.loading)  {
            viewModel.fetchPosts()
        }
        
    }
    
    // MARK: - Navigation

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "PostDetailSegue" {
            let vc = segue.destination as! PostDetailViewController
            vc.post = selectedPost
        }
    }
    

}

extension PostsTableViewController: PostsCellDelegate {
    func didTapLike(post: Post) {
        //selectedPost = post
    }
    
    func didTapCommment(post: Post) {
        selectedPost = post
        performSegue(withIdentifier: "PostDetailSegue", sender: self)
    }
    
    func didTapShare(post: Post, image: UIImage?) {
        var items: [Any] = post.getPostShareItems()
        if let image = image { items.append(image) }
        
        DispatchQueue.main.async {
            let vc = UIActivityViewController(activityItems: items, applicationActivities: nil)
            self.present(vc, animated: true)
        }
        
    }
    
}

extension PostsTableViewController: PostsViewModelDelegate {
    func loader(_ loading: Bool) {
        DispatchQueue.main.async {
            
            if self.postsRefreshControl.isRefreshing {
                self.postsRefreshControl.endRefreshing()
            }
            
            self.tableView.tableFooterView?.isHidden = loading ? false : true
        }
    }
    
    func posts(didChange posts: [Post]) {
        DispatchQueue.main.async {
            self.tableView.reloadData()
        }
    }
    
    func posts(didFailed errorMessage: String) {
        DispatchQueue.main.async {
            self.view.makeToast(errorMessage, style: .error)
        }
    }
    
}
