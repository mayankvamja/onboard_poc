//
//  PostsViewModel.swift
//  POC
//
//  Created by Mayank Vamja on 11/04/21.
//

import Foundation

protocol PostsViewModelDelegate {
    func loader(_ loading: Bool)
    func posts(didChange posts: [Post])
    func posts(didFailed errorMessage: String)
}

class PostsViewModel {

    var delegate: PostsViewModelDelegate?
    var posts = [Post]()
    
    private var page = 0
    private var pageSize = 10
    private let http: HttpUtils
    
    var loading = false {didSet { delegate?.loader(loading) }}
    
    init(config: URLSessionConfiguration = .default) {
        //config.protocolClasses = [AppIdInterceptor.self]
        http = HttpUtils(config: config)
    }
    
    func fetchPosts(reset: Bool = false) {
        
        if(reset) {
            loading = false
            page = 0
        }
        
        guard !loading else { return }
        
        loading = true
        page = page + 1
        let params = [ "page": "\(page)", "limit": "\(pageSize)" ]
          print(params)
        http.makeRequest(from: Endpoints.posts, parameters: params) { (data, response, error) in

            if let error = error {
                self.handleApiError(error: error)
                return
            }
            
            if let response = response as? HTTPURLResponse, let data = data {
                if(response.statusCode == 200) {
                    try? self.parseJson(from: data, reset: reset)
                    return
                } else {
                    self.delegate?.posts(didFailed: "Unable to fetch posts.".localized)
                    self.loading = false
                }
            }
            
        }
    }
    
    func parseJson(from data: Data, reset: Bool) throws {
        let postsResponse = try JSONDecoder().decode(PostsResponse.self, from: data)
        if (reset){
            self.posts = postsResponse.data
        } else {
            self.posts += postsResponse.data
        }
                
        delegate?.posts(didChange: posts)
        loading = false
    }
    
    func handleApiError(error: Error) {
        delegate?.posts(didFailed: error.localizedDescription)
        loading = false
    }
}
