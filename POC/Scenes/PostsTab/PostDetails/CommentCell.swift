//
//  CommentCell.swift
//  POC
//
//  Created by Mayank Vamja on 12/04/21.
//

import Foundation

import UIKit

class CommentCell: UITableViewCell {

    @IBOutlet var postOwnerImage: LazyImageView!
    @IBOutlet var postOwnerName: UILabel!
    @IBOutlet var postOwnerEmailLabel: UILabel!
    @IBOutlet var commentLabel: UILabel!
    @IBOutlet var commentDateLabel: UILabel!
    
    func config(with comment: Comment) {
        postOwnerImage.loadImage(from: comment.owner.picture)
        
        postOwnerName.text = "\(comment.owner.firstName) \(comment.owner.lastName)"
        postOwnerEmailLabel.text = comment.owner.email
        
        commentLabel.text = comment.message
        commentDateLabel.text = DateUtils.timeAgo(from: comment.publishDate)
    }
    
}
