//
//  PostDetailViewModel.swift
//  POC
//
//  Created by Mayank Vamja on 12/04/21.
//

import Foundation

protocol PostDetailViewModelDelegate {
    func loader(_ loading: Bool)
    func postDetail(didLoadComments changed: Bool)
    func postDetail(didFailed errorMessage: String)
}

class PostDetailViewModel {
    
    var delegate: PostDetailViewModelDelegate?
    
    var http: HttpUtils
    var comments : [Comment]
    var fetchCommentsOperation: Operation?
    
    init(config: URLSessionConfiguration = .default) {
        http = HttpUtils(config: config)
        comments = []
    }
    
    func fetchComments(from postId: String) {
        delegate?.loader(true)
        let url = "\(Endpoints.posts)/\(postId)/comment"
        
        http.makeRequest(from: url) { (data, response, error) in
            
            if let error = error {
                self.handleApiError(error: error)
                return
            }
            
            if let response = response as? HTTPURLResponse, let data = data {
                if(response.statusCode == 200) {
                   try? self.parseJson(from: data)
                } else {
                    self.delegate?.postDetail(didFailed: "Unable to fetch post comments".localized)
                    self.delegate?.loader(false)
                }
            }

        }
        
    }
    
    func parseJson(from data: Data) throws {
        let result = try JSONDecoder().decode(CommentsResponse.self, from: data)
        self.comments = result.data

        self.delegate?.postDetail(didLoadComments: true)
        self.delegate?.loader(false)
    }
    
    func handleApiError(error: Error) {
        self.delegate?.postDetail(didFailed: error.localizedDescription)
        self.delegate?.loader(false)
    }
}
