//
//  PostDetailViewController.swift
//  POC
//
//  Created by Mayank Vamja on 12/04/21.
//

import UIKit

class PostDetailViewController: UIViewController {

    @IBOutlet var commentsTableView: UITableView!
    
    var viewModel: PostDetailViewModel!
    var refreshControl = UIRefreshControl()
    var post: Post!

    override func viewDidLoad() {
        super.viewDidLoad()
        
        viewModel = PostDetailViewModel()
        viewModel.delegate = self
        viewModel.fetchComments(from: post.id)
        
        commentsTableView.refreshControl = refreshControl
        refreshControl.addTarget(self, action: #selector(handleRefreshControl), for: .valueChanged)
        
    }
    
    @objc func handleRefreshControl() {
        guard let post = post else { return }
        viewModel.fetchComments(from: post.id)
    }
    
}

extension PostDetailViewController: PostDetailViewModelDelegate {
    func loader(_ loading: Bool) {
        DispatchQueue.main.async {
            if (!loading) { self.refreshControl.endRefreshing() }
        }
    }
    
    func postDetail(didLoadComments changed: Bool) {
        DispatchQueue.main.async {
            self.commentsTableView.reloadData()
        }
    }
    
    func postDetail(didFailed errorMessage: String) {
        DispatchQueue.main.async {
            self.view.makeToast(errorMessage)
        }
    }
    
}

extension PostDetailViewController: UITableViewDelegate, UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int { 2 }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return section == 0 ? 1 : viewModel.comments.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if indexPath.section == 0, let post = post {
            let cell = tableView.dequeueReusableCell(withIdentifier: "post_detail_cell", for: indexPath) as! PostsCell
            cell.config(with: post)
            cell.delegate = self
            return cell
            
        } else {
            let cell = tableView.dequeueReusableCell(withIdentifier: "post_comment_cell", for: indexPath) as! CommentCell
            cell.config(with: viewModel.comments[indexPath.row])
            
            return cell
        }
    }
}

extension PostDetailViewController: PostsCellDelegate {
    func didTapLike(post: Post) {}
    func didTapCommment(post: Post) {}
    
    func didTapShare(post: Post, image: UIImage?) {
        var items: [Any] = post.getPostShareItems()
        if let image = image { items.append(image) }
        
        DispatchQueue.main.async {
            let vc = UIActivityViewController(activityItems: items, applicationActivities: nil)
            self.present(vc, animated: true)
        }
    }
    
    
}
