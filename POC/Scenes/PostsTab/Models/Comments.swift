//
//  Comments.swift
//  POC
//
//  Created by Mayank Vamja on 12/04/21.
//

import Foundation

// MARK: - Welcome
struct CommentsResponse: Codable {
    let data: [Comment]
    let total, page, limit, offset: Int
}

// MARK: - Datum
struct Comment: Codable {
    let owner: Owner
    let id, message, publishDate: String
}
