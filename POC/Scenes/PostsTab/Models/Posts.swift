//
//  Posts.swift
//  POC
//
//  Created by Mayank Vamja on 12/04/21.
//

import Foundation


// MARK: - Welcome
struct PostsResponse: Codable {
    let data: [Post]
    let total, page, limit, offset: Int
}

// MARK: - Datum
struct Post: Codable {
    let owner: Owner
    let id: String
    let image: String
    let publishDate, text: String
    let tags: [String]
    let link: String?
    let likes: Int
    
    func getPostShareItems() -> [Any] {
        var items: [Any] = ["\(self.text) \n\t - Shared By \(self.owner.firstName) \(self.owner.lastName)"]
        if let link = self.link, let url = URL(string: link) {
            items.append(url)
        }
        
        return items
    }
}

// MARK: - Owner
struct Owner: Codable {
    let id, lastName: String
    let title: String
    let email: String
    let picture: String
    let firstName: String
}

enum Title: String, Codable {
    case miss = "miss"
    case mr = "mr"
    case mrs = "mrs"
}
