//
//  SignupViewController.swift
//  POC
//
//  Created by Mayank Vamja on 22/04/21.
//

import UIKit
import Combine

class SignupViewController: UIViewController {
    
    @IBOutlet var firstNameErrorLabel: UILabel!
    @IBOutlet var lastNameErrorLabel: UILabel!
    @IBOutlet var phoneNumberErrorLabel: UILabel!
    @IBOutlet var emailErrorLabel: UILabel!
    @IBOutlet var dobErrorLabel: UILabel!
    @IBOutlet var passwordEErrorLabel: UILabel!
    @IBOutlet var confirmPasswordErrorLabel: UILabel!
    
    @IBOutlet var birthDatePicker: UIDatePicker!
    @IBOutlet var passwordStrengthLabel: UILabel!
    @IBOutlet var signupButton: UIButton!
    
    private var isSignupEnabled: Bool = false {
        didSet {
            DispatchQueue.main.async {
                self.signupButton.isEnabled = self.isSignupEnabled
                self.signupButton.alpha = self.isSignupEnabled ? 1.0 : 0.5
            }
        }
    }
    
    private var passwordStrength: PasswordStrength? {
        didSet {
            guard let value = passwordStrength else {
                DispatchQueue.main.async {
                    self.passwordStrengthLabel.text = ""
                }
                return
            }
            
            DispatchQueue.main.async {
                self.passwordStrengthLabel.text = value.label
                self.passwordStrengthLabel.textColor = value.color
            }
        }
    }
    
    private var showHideLoader: Bool? {
        didSet {
            guard let value = showHideLoader else { return }
            
            DispatchQueue.main.async {
                value ? self.showLoader() : self.dismissLoader()
            }
        }
    }
    
    var subscriptions = Set<AnyCancellable>()
    var keyboardObservers:  [AnyObject]?
    var viewModel: SignupViewModel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        viewModel = SignupViewModel()
        
        // Minimum age == 5
        birthDatePicker.maximumDate = Calendar.current.date(byAdding: .year, value: -5, to: Date())

        setupBinding()

        keyboardObservers = SafeKeyBoard.listeners(show: {
            self.additionalSafeAreaInsets = $0
        }, hide: {
            self.additionalSafeAreaInsets = $0
        })

    }
    
    func setupBinding() {
        
        viewModel.onSignupResult = { (success: Bool, message: String) in
            if (success) {
                self.view.makeToast(message, style: .success)
                self.dismiss(animated: true, completion: nil)
            } else {
                self.view.makeToast(message, style: .error)
            }
        }
        
        subscriptions = [
            viewModel.$firstNameError.assign(to: \.text, on: firstNameErrorLabel),
            viewModel.$lastNameError.assign(to: \.text, on: lastNameErrorLabel),
            viewModel.$phoneNumberError.assign(to: \.text, on: phoneNumberErrorLabel),
            viewModel.$emailError.assign(to: \.text, on: emailErrorLabel),
            viewModel.$birthDateError.assign(to: \.text, on: dobErrorLabel),
            viewModel.$passwordError.assign(to: \.text, on: passwordEErrorLabel),
            viewModel.$confirmPasswordError.assign(to: \.text, on: confirmPasswordErrorLabel),
            viewModel.$canSignup.assign(to: \.isSignupEnabled, on: self),
            viewModel.$passwordStrength.assign(to: \.passwordStrength, on: self),
            viewModel.$loading.assign(to: \.showHideLoader, on: self),
        ]
    }
    
    
    // MARK: TextField Methods
    
    @IBAction func firstNameChanged(_ sender: UITextField) {
        viewModel.checkForFirstName(text: sender.text)
    }
    
    @IBAction func lastNameChanged(_ sender: UITextField) {
        viewModel.checkForLastName(text: sender.text)
    }
    
    @IBAction func emailChanged(_ sender: UITextField) {
        viewModel.checkForEmail(text: sender.text)
    }
    
    @IBAction func phoneNumberChanged(_ sender: UITextField) {
        viewModel.checkForPhoneNumber(text: sender.text)
    }
    
    @IBAction func passwordChanged(_ sender: UITextField) {
        viewModel.checkForPassword(text: sender.text)
    }
    
    @IBAction func confirmPasswordChanged(_ sender: UITextField) {
        viewModel.checkForConfirmPassword(text: sender.text)
    }

    @IBAction func birthDateChanged(_ sender: UIDatePicker) {
        viewModel.checkForBirthDate(date: sender.date)
    }
    
    // MARK: Change Border color of text fields
    
    @IBAction func fieldFocussed(_ sender: UITextField) {
        sender.borderColor = UIColor(named: "AppColor")
    }
    
    @IBAction func fieldLostFocus(_ sender: UITextField) {
        sender.borderColor = .lightGray
    }
    
    
    // MARK: onSignup
    
    @IBAction func onSignup() {
        self.view.endEditing(true)
        viewModel.registerUser()
    }
    
    // MARK: goBackToLogin
    
    @IBAction func goBackToLogin() {
        self.dismiss(animated: true)
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
    }
    
    func showLoader() {
        self.view.makeToastActivity(.center)
    }
    
    func dismissLoader() {
        self.view.hideToastActivity()
    }
    
    deinit {
        SafeKeyBoard.removeListeners(keyboardObservers!)
    }
}
