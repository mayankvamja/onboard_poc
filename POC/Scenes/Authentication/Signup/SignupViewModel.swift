//
//  SignupViewModel.swift
//  POC
//
//  Created by Mayank Vamja on 23/04/21.
//

import Foundation

class SignupViewModel {
    
    var email: String = ""
    var password: String = ""
    var confirmPassword: String = ""
    var firstName: String = ""
    var lastName: String = ""
    var phoneNumber: String = ""
    var birthDate: Date? = Calendar.current.date(byAdding: .year, value: -5, to: Date())

    @Published private(set) var firstNameError: String?
    @Published private(set) var lastNameError: String?
    @Published private(set) var emailError: String?
    @Published private(set) var birthDateError: String?
    @Published private(set) var phoneNumberError: String?
    @Published private(set) var passwordError: String?
    @Published private(set) var confirmPasswordError: String?
    
    @Published private(set) var passwordStrength: PasswordStrength?
    @Published private(set) var canSignup: Bool = false
    
    @Published private(set) var loading: Bool?
    var onSignupResult: ((Bool, String) -> Void)?

    private let validation = ValidationService()

    func checkForFirstName(text: String?) {
        do {
            try validation.validateFirstName(text: text)
            
            firstNameError = nil
            firstName = text!
            checkForSignup()
        } catch {
            firstNameError = error.localizedDescription
        }
    }
    
    func checkForLastName(text: String?) {
        do {
            try validation.validateLastName(text: text)
            
            lastNameError = nil
            lastName = text!
            checkForSignup()
        } catch {
            lastNameError = error.localizedDescription
        }
    }
    
    func checkForPhoneNumber(text: String?) {
        do {
            try validation.validatePhoneNumber(text: text)
            
            phoneNumberError = nil
            phoneNumber = text!
            checkForSignup()
        } catch {
            phoneNumberError = error.localizedDescription
        }
    }
    
    func checkForEmail(text: String?) {
        do {
            try validation.validateNewEmail(text: text)
            
            emailError = nil
            email = text!
            checkForSignup()
        } catch {
            emailError = error.localizedDescription
        }
    }
    
    func checkForBirthDate(date: Date) {
        birthDate = date
        
        /*do {
            try validation.validateBirthDate(date: date)
            
            birthDateError = nil
            birthDate = date
            checkForSignup()
        } catch {
            birthDateError = error.localizedDescription
        }*/
    }
    
    func checkForPassword(text: String?) {
        do {
            try validation.validateNewPassword(text: text)
            
            passwordError = nil
            password = text!
            passwordStrength = validation.checkPasswordStrength(text: text!)
            
            checkForSignup()
        } catch {
            passwordError = error.localizedDescription
        }
    }
    
    func checkForConfirmPassword(text: String?) {
        do {
            try validation.validateConfirmPassword(text: text, password: password)
            
            confirmPasswordError = nil
            confirmPassword = text!
            checkForSignup()
        } catch {
            confirmPasswordError = error.localizedDescription
        }
    }
    
    func checkForSignup() {
        guard birthDate != nil else { return }
        
        canSignup = (
            firstNameError == nil &&
            lastNameError == nil &&
            emailError == nil &&
            phoneNumberError == nil &&
            passwordError == nil &&
            confirmPasswordError == nil &&
            firstName.count > 0 &&
            lastName.count > 0 &&
            email.count > 0 &&
            password.count > 0 &&
            confirmPassword.count > 0
        )
    }
    
    func registerUser() {
        loading = true
        
        let newUser = AuthUser(context: AuthDataStore.shared.context)
        newUser.id = "\(Date().timeIntervalSince1970)"
        newUser.email = email
        newUser.firstName = firstName
        newUser.lastName = lastName
        newUser.phoneNumber = phoneNumber
        newUser.password = password
        newUser.dateOfBirth = birthDate
        
        AuthDataStore.shared.save { (saved) in
            self.loading = false

            if (saved) {
                self.onSignupResult?(true, "Sign up successful".localized)
            } else {
                self.onSignupResult?(false, "Sign up failed. Try again...".localized)
            }
        }
    }

}
