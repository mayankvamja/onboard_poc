//
//  VerificationViewController.swift
//  POC
//
//  Created by Mayank Vamja on 02/05/21.
//

import UIKit
import LocalAuthentication
import Combine

class VerificationViewModel {
    
    @Published private(set) var username: String?
    @Published private(set) var userEmail: String?
    @Published private(set) var biometricsButtonHidden: Bool = false
    
    var verificationSuccess: (() -> Void)?
    var verificationFailed: ((String) -> Void)?
    
    func checkBioMetricsAvailability() {
        let context = LAContext()
        var error: NSError?
        
        if context.canEvaluatePolicy(.deviceOwnerAuthenticationWithBiometrics, error: &error) {
            biometricsButtonHidden = false
            return
        }
        
        biometricsButtonHidden = true
    }
    
    func verifyBiometrics() {
        LAContext().evaluatePolicy(.deviceOwnerAuthenticationWithBiometrics, localizedReason: "Verify yourself".localized) { [weak self] (success, authenticationError) in
            if success {
                self?.verificationSuccess?()
            } else {
                self?.verificationFailed?(authenticationError?.localizedDescription ?? "Biometrics verification failed".localized)
            }
        }
    }
    
    func fetchLoggedInUserDetails() {
        if let user = AuthDataStore.shared.loggedInUser() {
            print(user)
            username = "\(user.firstName!) \(user.lastName!)"
            userEmail = user.email!
        }
    }
    
    func verify(_ text: String) -> Bool {
        if let user = AuthDataStore.shared.loggedInUser(), text == user.password {
            return true
        }
        return false
    }
}

class VerificationViewController: UIViewController {

    @IBOutlet var nameLabel: UILabel!
    @IBOutlet var emailLabel: UILabel!
    @IBOutlet var passwordField: UITextFieldPadded!
    @IBOutlet var biometricsInfoLabel: UILabel!
    @IBOutlet var biometricVerificationButton: UIButton!

    let viewModel = VerificationViewModel()
    var subscriptions = Set<AnyCancellable>()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupBindings()
        viewModel.fetchLoggedInUserDetails()
        viewModel.checkBioMetricsAvailability()
        
        if !viewModel.biometricsButtonHidden {
            viewModel.verifyBiometrics()
        }
    }
    
    func setupBindings() {
        subscriptions = [
            viewModel.$userEmail.assign(to: \.text, on: emailLabel),
            viewModel.$username.assign(to: \.text, on: nameLabel),
            viewModel.$biometricsButtonHidden.assign(to: \.isHidden, on: biometricVerificationButton),
            viewModel.$biometricsButtonHidden.assign(to: \.isHidden, on: biometricsInfoLabel)
        ]
        
        viewModel.verificationSuccess = {
            DispatchQueue.main.async {
                self.view.makeToast("Verified", style: .success)
                self.onSuccessfullVerification()
            }
        }
        
        viewModel.verificationFailed = { message in
            DispatchQueue.main.async {
                DispatchQueue.main.async {
                    self.view.makeToast(message, style: .error)
                }
            }
        }
    }
    
    @IBAction func onVerifyTapped(_ sender: Any) {
        guard passwordField.text?.count ?? 0 > 0 else {
            self.view.makeToast("Please enter password.", style: .error)
            return
        }
        
        if viewModel.verify(passwordField.text!) {
            self.view.makeToast("Verified", style: .success)
            onSuccessfullVerification()
        } else {
            self.view.makeToast("Wring password", style: .error)
        }
        
    }
    
    @IBAction func onBiometricLockTapped() {
        viewModel.verifyBiometrics()
    }
    
    func onSuccessfullVerification() {
        let vc  = UIStoryboard(name: "Main", bundle: nil).instantiateInitialViewController()
        let window = (UIApplication.shared.connectedScenes.first as! UIWindowScene).windows.first!
        window.rootViewController = vc
    }
}
