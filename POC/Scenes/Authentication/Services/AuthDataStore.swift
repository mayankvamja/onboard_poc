//
//  AuthDataStore.swift
//  POC
//
//  Created by Mayank Vamja on 24/04/21.
//

import Foundation

import CoreData
import UIKit

struct StorageKeys {

    static let IsLoggedIn = "IS_LOGGED_IN"
    static let LoggedInUserId = "LOGGED_IN_USER_ID"
    static let LoginTimestamp = "LOGIN_TIME_STAMP"
    
}

class AuthDataStore {

    static let shared = AuthDataStore()
    
    let storage = UserDefaults.standard
    
    let context: NSManagedObjectContext
    let container: NSPersistentContainer = {
        let container = NSPersistentContainer(name: "Users")
        container.loadPersistentStores(completionHandler: { (storeDescription, error) in
            if let error = error as NSError? {
                debugPrint("Unresolved error \(error.localizedDescription), \(error.userInfo)")
            }
        })
        return container
    }()
    
    init() {
        context = container.viewContext
    }
    
    func save(completion: ((Bool) -> Void)? = nil) {

        guard context.hasChanges else { return }
        
        do {
            try context.save()
            completion?(true)
        } catch {
            completion?(false)
            debugPrint("Error saving context: \(error.localizedDescription)")
        }
    }
    
    func isEmailAvailable(email: String) -> Bool {
        
        let request: NSFetchRequest<AuthUser> = AuthUser.fetchRequest()
        request.predicate = NSPredicate(format: "email MATCHES[c] %@", email)
        
        guard let foundUser = try? context.fetch(request),
           foundUser.count == 0 else {
            return false
        }
        
        return true
    }
    
    func login(email: String, password: String) -> Bool {
        
        let request: NSFetchRequest<AuthUser> = AuthUser.fetchRequest()
        request.predicate = NSPredicate(format: "email MATCHES[c] %@ AND password MATCHES[c] %@", email, password)
        
        guard let foundUser = try? context.fetch(request),
           foundUser.count == 1 else {
          return false
        }
        
        foundUser[0].isLoggedIn = true
        save()
        
        storage.set(true, forKey: StorageKeys.IsLoggedIn)
        storage.set(foundUser[0].id, forKey: StorageKeys.LoggedInUserId)
        storage.set("\(Date().timeIntervalSince1970)", forKey: StorageKeys.LoginTimestamp)

        return true
    }
    
    func loggedInUser() -> AuthUser? {
        
        let loggedInUserId = storage.string(forKey: StorageKeys.LoggedInUserId)
        
        let request: NSFetchRequest<AuthUser> = AuthUser.fetchRequest()
        request.predicate = NSPredicate(format: "id MATCHES %@", loggedInUserId ?? "-")
                
        guard let foundUser = try? context.fetch(request),
           foundUser.count == 1 else {
          return nil
        }
        
        return foundUser[0]
    }
    
    func deleteUser(by email: String, completion: ((Bool) -> Void)? = nil) {
        let request: NSFetchRequest<AuthUser> = AuthUser.fetchRequest()
        request.predicate = NSPredicate(format: "email MATCHES[c] %@", email)
        
        guard let foundUser = try? context.fetch(request),
           foundUser.count > 0 else {
            return
        }
        
        foundUser.forEach { context.delete($0) }
        save(completion: completion)
    }
    
    func logout() -> Bool {
        
        if let user = loggedInUser() {
            user.isLoggedIn = false
            save()
        }
        
        storage.removeObject(forKey: StorageKeys.IsLoggedIn)
        storage.removeObject(forKey: StorageKeys.LoggedInUserId)
        storage.removeObject(forKey: StorageKeys.LoginTimestamp)

        return true
    }
    
}
