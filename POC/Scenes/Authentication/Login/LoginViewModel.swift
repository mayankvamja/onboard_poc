//
//  LoginViewModel.swift
//  POC
//
//  Created by Mayank Vamja on 22/04/21.
//

import Foundation

class LoginViewModel {
    
    var email: String = ""
    var password: String = ""
    
    @Published private(set) var emailError: String?
    @Published private(set) var passwordError: String?
    @Published private(set) var canTapLogin: Bool = false
    @Published private(set) var loading: Bool?

    var onLoginResult: ((Bool, String) -> Void)?
    private let validation = ValidationService()
    
    func checkForEmail(text: String?) {
        do {
            try validation.validateEmail(text: text)
            email = text!
            emailError = nil
            
            checkForLoginButton()
        } catch {
            emailError = error.localizedDescription
        }
    }
    
    func checkForPassword(text: String?) {
        do {
            try validation.validatePassword(text: text)
            password = text!
            passwordError = nil
            
            checkForLoginButton()
        } catch {
            passwordError = error.localizedDescription
        }
    }
    
    func checkForLoginButton() {
        canTapLogin = (emailError == nil && passwordError == nil && email.count > 0 && password.count > 0) ? true : false
    }
    
    func login() {
                
        if AuthDataStore.shared.login(email: email, password: password) {
            onLoginResult?(true, "Login successful".localized)
        } else {
            onLoginResult?(false, "Invalid login credentials. Try again...".localized)
        }
    }
}
