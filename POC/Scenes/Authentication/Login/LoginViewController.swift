//
//  LoginViewController.swift
//  POC
//
//  Created by Mayank Vamja on 22/04/21.
//

import UIKit
import Combine

class LoginViewController: UIViewController {
    
    @IBOutlet var emailField: UITextFieldPadded!
    @IBOutlet var emailErrorLabel: UILabel!
    
    @IBOutlet var passwordField: UITextFieldPadded!
    @IBOutlet var passwordErrorLabel: UILabel!
    
    @IBOutlet var loginButton: UIButton!
    var isLoginEnabled: Bool = false {
        didSet {
            DispatchQueue.main.async {
                self.loginButton.isEnabled = self.isLoginEnabled
                self.loginButton.alpha = self.isLoginEnabled ? 1.0 : 0.5
            }
        }
    }
    
    private var showHideLoader: Bool? {
        didSet {
            guard let value = showHideLoader else { return }
            
            DispatchQueue.main.async {
                value ? self.showLoader() : self.dismissLoader()
            }
        }
    }
    
    var subscriptions = Set<AnyCancellable>()
    var viewModel: LoginViewModel!
    var keyboardObservers:  [AnyObject]?

    override func viewDidLoad() {
        super.viewDidLoad()
        
        viewModel = LoginViewModel()
        setupBindings()
        
        keyboardObservers = SafeKeyBoard.listeners(show: {
            self.additionalSafeAreaInsets = $0
        }, hide: {
            self.additionalSafeAreaInsets = $0
        })
    }
    
    func setupBindings() {
        
        viewModel.onLoginResult = { (success: Bool, message: String) in
            if (success) {
                self.view.makeToast(message, style: .success)

                let main = UIStoryboard(name: "Main", bundle: nil)
                
                let mainVC = main.instantiateInitialViewController()!
                mainVC.modalPresentationStyle = .fullScreen
                
                self.present(mainVC, animated: true, completion: nil)
            } else {
                self.view.makeToast(message, style: .error)
            }
        }
        
        subscriptions = [
            viewModel.$emailError.assign(to: \.text, on: emailErrorLabel),
            viewModel.$passwordError.assign(to: \.text, on: passwordErrorLabel),
            viewModel.$canTapLogin.assign(to: \.isLoginEnabled, on: self),
            viewModel.$loading.assign(to: \.showHideLoader, on: self),
        ]
        
    }
    
    
    @IBAction func emailFieldChanged(_ sender: UITextField) {
        viewModel.checkForEmail(text: sender.text)
    }
    
    @IBAction func passwordFieldChanged(_ sender: UITextField) {
        viewModel.checkForPassword(text: sender.text)
    }
    
    
    // MARK: Change Border color of text fields
    
    @IBAction func fieldFocussed(_ sender: UITextField) {
        sender.borderColor = UIColor(named: "AppColor")
    }
    
    @IBAction func fieldLostFocus(_ sender: UITextField) {
        sender.borderColor = .lightGray
    }
    
    // MARK: onLogin
    
    @IBAction func loginTapped() {
        viewModel.login()
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
    }
    
    func showLoader() {
        self.view.makeToastActivity(.center)
    }
    
    func dismissLoader() {
        self.view.hideToastActivity()
    }
    
    deinit {
        SafeKeyBoard.removeListeners(keyboardObservers!)
    }
}
