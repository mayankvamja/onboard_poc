//
//  UtilsViewModel.swift
//  POC
//
//  Created by Mayank Vamja on 25/04/21.
//

import Foundation
import Combine
import UserNotifications

class UtilsViewModel {
    
    let notificationService: NotificationService
    var onNotificationAdded: (() -> Void)?
    var onNotificationFailed: ((_ errorMessage: String) -> Void)?
    
    var onUpdateDeliveredNotificationsCount: ((Int) -> Void)?
    var onUpdateScheduledNotificationsCount: ((Int) -> Void)?
    
    @Published private(set) var deliveredNotifications: [UNNotification]?
    @Published private(set) var pendingNotifications: [UNNotificationRequest]?

    init() {
        notificationService = NotificationService()
    }
    
    func checkAuthorization() {
        NotificationService.authorizationStatus { (status) in
            if status == .notDetermined {
                NotificationService.requestAuthorization()
            } else if status == .denied {
                self.onNotificationFailed?("Please turn on notifications in settings.".localized)
            }
        }
    }
    
    func addNewNotification(title: String?, body: String, image: String?, datetime: Date) {
        
        if ((title == nil || title?.count == 0) && body.count == 0) {
            self.onNotificationFailed?("Please provide title or body.".localized)
            return
        }
        
        var newDatetime: Date = datetime
        
        if (datetime.timeIntervalSinceNow <= 0) {
            newDatetime = Date(timeIntervalSinceNow: 5)
        }
        
        let item = LocalNotificationItem(category: .alert, title: title, subtitle: nil, body: body, image: URL(string: image ?? ""), datetime: newDatetime)
        
        
        notificationService.localNotification(item: item) { (success, error) in
            
            if success {
                self.onNotificationAdded?()
                self.fetchScheduledNotifications()
                return
            }
            
            print(error?.localizedDescription as Any)
            self.onNotificationFailed?("Unable to schdule notification.".localized)
        }
        
    }
    
    
    func fetchDeliveredNotifications() {
        
        NotificationService.center.getDeliveredNotifications { (notifications) in
            self.deliveredNotifications = notifications
            self.onUpdateDeliveredNotificationsCount?(notifications.count)
        }
        
    }
    
    func fetchScheduledNotifications() {
        
        NotificationService.center.getPendingNotificationRequests(completionHandler: { (requests) in
            self.pendingNotifications = requests
            self.onUpdateScheduledNotificationsCount?(requests.count)
        })
        
    }
    
    func clearNotificationCenter() {
        NotificationService.center.removeAllDeliveredNotifications()
        
        self.deliveredNotifications = []
        self.onUpdateDeliveredNotificationsCount?(0)

    }
    
    func clearNotification(with identifier: String) {
        NotificationService.center.removeDeliveredNotifications(withIdentifiers: [identifier])
        self.fetchDeliveredNotifications()
    }
    
    func cancelAllNotifications() {
        NotificationService.center.removeAllPendingNotificationRequests()
        
        self.pendingNotifications = []
        self.onUpdateScheduledNotificationsCount?(0)

    }
    
    func cancelNotification(with identifier: String) {
        NotificationService.center.removePendingNotificationRequests(withIdentifiers: [identifier])
        self.fetchScheduledNotifications()
    }
    
}
