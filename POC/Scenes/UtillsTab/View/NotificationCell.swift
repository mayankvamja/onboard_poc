//
//  NotificationCell.swift
//  POC
//
//  Created by Mayank Vamja on 26/04/21.
//

import UIKit

class NotificationCell: UITableViewCell {
    
    @IBOutlet var titleLabel: UILabel!
    @IBOutlet var imageAttachement: LazyImageView!
    @IBOutlet var bodyLabel: UILabel!
    @IBOutlet var datetimeLabel: UILabel!
    
    func config(with content: UNNotificationContent, date: Date?) {
        titleLabel.text = content.title
        bodyLabel.text = content.body
        
        if let date = date {
            if date.timeIntervalSinceNow < 0 {
                datetimeLabel.text = DateUtils.timeAgo(from: date)
            } else {
                datetimeLabel.text = DateUtils.timeAfter(from: date)
            }
        } else {
            datetimeLabel.text = nil
        }
        
        if let imageContent = content.attachments.first {
            imageAttachement.loadImage(from: imageContent.url, with: UIImage(named: "placeholder"))
        } else {
            imageAttachement.isHidden = true
        }
    }
    
}
