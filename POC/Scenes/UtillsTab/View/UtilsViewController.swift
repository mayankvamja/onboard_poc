//
//  UtilsViewController.swift
//  POC
//
//  Created by Mayank Vamja on 25/04/21.
//

import UIKit
import Combine

class UtilsViewController: UITableViewController {

    @IBOutlet var notificationTitleField: UITextField!
    @IBOutlet var imageUrlField: UITextField!
    
    @IBOutlet var notificationBodyTextView: UITextView!
    @IBOutlet var notificationDateTimePicker: UIDatePicker!
    
    @IBOutlet var addNotificationButton: UIButton!
    
    @IBOutlet var notificationHistoryLabel: UILabel!
    @IBOutlet var scheduledNotificationsLabel: UILabel!
    
    private var viewModel: UtilsViewModel!
    private var subscriptions = Set<AnyCancellable>()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        viewModel = UtilsViewModel()
        setupBinding()
        
        viewModel.checkAuthorization()
        viewModel.fetchScheduledNotifications()
        viewModel.fetchDeliveredNotifications()
        
    }
    
    func setupBinding() {
        
        viewModel.onNotificationAdded = {
            DispatchQueue.main.async {
                self.parent?.view.makeToast("Notification added successfully".localized, duration: 3.0, position: .bottom, style: .primary)
            }
        }
        
        viewModel.onNotificationFailed = { message in
            DispatchQueue.main.async {
                self.parent?.view.makeToast(message, duration: 3.0, position: .bottom, style: .error)
            }
        }
        
        viewModel.onUpdateDeliveredNotificationsCount = { count in
            DispatchQueue.main.async {
                self.notificationHistoryLabel.text! = "Notifications History".localized + " (\(count))"
            }
        }
        
        viewModel.onUpdateScheduledNotificationsCount = { count in
            DispatchQueue.main.async {
                self.scheduledNotificationsLabel.text! = "Scheduled Notifications".localized + " (\(count))"
            }
        }
        
    }
    
    
    override func viewDidAppear(_ animated: Bool) {
        // Minimum 30 seconds from now
        notificationDateTimePicker.minimumDate = Date(timeIntervalSinceNow: 30)
    }

    @IBAction func onAddNotificationTapped() {
        self.view.endEditing(true)
        
        let title = notificationTitleField.text
        let body = notificationBodyTextView.text
        let image = imageUrlField.text
        let datetime = notificationDateTimePicker.date
        
        viewModel.addNewNotification(title: title, body: body!, image: image, datetime: datetime)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        switch segue.identifier {
        case "NotificationsHistorySegue":
            let vc = segue.destination as! NotificationHistoryViewController
            vc.viewModel = viewModel
            
        case "PendingNotificationsSegue":
            let vc = segue.destination as! PendingNotificationsViewController
            vc.viewModel = viewModel
            
        default:
            break
        }
    }
}
