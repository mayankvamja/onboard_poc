//
//  NotificationHistoryViewController.swift
//  POC
//
//  Created by Mayank Vamja on 26/04/21.
//

import UIKit
import Combine

class NotificationHistoryViewController: UITableViewController {
    
    @IBOutlet var noDataViewWrapper: UIView!
    @IBOutlet var clearAllButton: UIButton!
    @IBOutlet var noDataView: UIStackView!
    
    var notifications: [UNNotification]? {
        didSet {
            DispatchQueue.main.async {
                self.tableView.reloadData()
                let isDataAvailable = self.notifications?.count ?? 0 > 0
                self.noDataViewWrapper.isHidden = isDataAvailable
                self.clearAllButton.isHidden = !isDataAvailable
            }
        }
    }
    
    var viewModel: UtilsViewModel?
    
    private var subscriptions = Set<AnyCancellable?>()

    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupBinding()

    }
    
    func setupBinding() {
        
        subscriptions = [
            viewModel?.$deliveredNotifications.assign(to: \.notifications, on: self)
        ]
        
    }

    @IBAction func clearAllNotificationsTapped() {
        viewModel?.clearNotificationCenter()
    }
    
    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return notifications?.count ?? 0
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "NotificationCell", for: indexPath) as! NotificationCell
        let notification = notifications![indexPath.row]
        
        cell.config(with: notification.request.content, date: notification.date)
        return cell
        
    }
    
    override func tableView(_ tableView: UITableView, leadingSwipeActionsConfigurationForRowAt indexPath: IndexPath)
            -> UISwipeActionsConfiguration? {
        return makeSwipeToDeleteConfiguration(indexPath)
    }
    
    override func tableView(_ tableView: UITableView, trailingSwipeActionsConfigurationForRowAt indexPath: IndexPath)
            -> UISwipeActionsConfiguration? {
        return makeSwipeToDeleteConfiguration(indexPath)
    }
    
    func makeSwipeToDeleteConfiguration(_ indexPath: IndexPath) -> UISwipeActionsConfiguration {
        
        let notification = notifications![indexPath.row]
        
        let deleteAction = UIContextualAction(style: .destructive, title: nil) { (_, _, completionHandler) in
            self.viewModel?.clearNotification(with: notification.request.identifier)
            completionHandler(true)
        }
        
        deleteAction.image = UIImage(systemName: "trash")
        deleteAction.backgroundColor = .systemRed
        
        return UISwipeActionsConfiguration(actions: [deleteAction])
    }


    /*
    // Override to support conditional editing of the table view.
    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the specified item to be editable.
        return true
    }
    */

    /*
    // Override to support editing the table view.
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            // Delete the row from the data source
            tableView.deleteRows(at: [indexPath], with: .fade)
        } else if editingStyle == .insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }    
    }
    */

    /*
    // Override to support rearranging the table view.
    override func tableView(_ tableView: UITableView, moveRowAt fromIndexPath: IndexPath, to: IndexPath) {

    }
    */

    /*
    // Override to support conditional rearranging of the table view.
    override func tableView(_ tableView: UITableView, canMoveRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the item to be re-orderable.
        return true
    }
    */

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
