//
//  NotificationService.swift
//  POC
//
//  Created by Mayank Vamja on 25/04/21.
//

import Foundation
import UserNotifications
import UIKit

enum NotificationCategory: String {
    case alert = "Local-Alert-Notification"
}

struct LocalNotificationItem {
    var category: NotificationCategory = .alert
    var title: String?
    var subtitle: String?
    var body: String?
    var image: URL?
    var datetime: Date
}

class NotificationService: NSObject, UNUserNotificationCenterDelegate {
    
    // MARK: Static Methods
    
    static let center = UNUserNotificationCenter.current()
    
    static func authorizationStatus(_ status: @escaping (UNAuthorizationStatus) -> Void) {
        center.getNotificationSettings { status($0.authorizationStatus) }
    }
    
    static func requestAuthorization() {
        center.requestAuthorization(options: [.alert, .badge, .sound]) { (allowed, error) in
            
            if allowed {
                debugPrint("Allowed Notifications")
            }
            if let error = error {
                debugPrint(error.localizedDescription)
            }
        }
    }
    
    
    // MARK: Instance Methods
    
    override init() {
        super.init()
        NotificationService.center.delegate = self
    }
    
    func localNotification(item: LocalNotificationItem, completion: @escaping (Bool, Error?) -> Void) {
        
        let content = UNMutableNotificationContent()
        
        content.categoryIdentifier = item.category.rawValue
        content.sound = .default
        
        if item.title != nil { content.title = item.title! }
        if item.subtitle != nil { content.subtitle = item.subtitle! }
        if item.body != nil { content.body = item.body! }
        
        let trigger = UNTimeIntervalNotificationTrigger(timeInterval: item.datetime.timeIntervalSinceNow, repeats: false)
        
        if let image = item.image {
            
            let task = URLSession.shared.downloadTask(with: image) { (result, response, error) in
                
                if let result = result {
                    
                    let identifier = ProcessInfo.processInfo.globallyUniqueString
                    let target = FileManager.default.temporaryDirectory.appendingPathComponent(identifier).appendingPathExtension(image.pathExtension)
                    
                    do {
                        try FileManager.default.moveItem(at: result, to: target)
                        
                        let attachment = try UNNotificationAttachment(identifier: identifier, url: target, options: nil)
                        content.attachments.append(attachment)
                        
                        let notification = UNNotificationRequest(identifier: Date().description, content: content, trigger: trigger)
                        
                        NotificationService.center.add(notification, withCompletionHandler: { (error) in
                            if let error = error {
                                print(error.localizedDescription)
                                completion(false, error)
                                return
                            } else {
                                completion(true, nil)
                                return
                            }
                        })
                    }
                    catch {
                        print(error.localizedDescription)
                        completion(false, error)
                        return
                    }
                } else {
                    completion(false, error)
                }
            }
            task.resume()
            
            
        } else {
            
            let notification = UNNotificationRequest(identifier: Date().description, content: content, trigger: trigger)
            
            NotificationService.center.add(notification, withCompletionHandler: { (error) in
                if let error = error {
                    print(error.localizedDescription)
                    completion(false, error)
                    return
                } else {
                    completion(true, nil)
                    return
                }
            })
            
        }
            
        
    }
    
    
    // MARK: Delegate Methods
    
    func userNotificationCenter(_ center: UNUserNotificationCenter, didReceive response: UNNotificationResponse, withCompletionHandler completionHandler: @escaping () -> Void) {
        
        switch response.notification.request.content.categoryIdentifier {
        
        case NotificationCategory.alert.rawValue:
            self.handleLocalNotification(response)
            break
            
        default:
            return
        }
        
        completionHandler()
    }
    
    
    func userNotificationCenter(_ center: UNUserNotificationCenter, willPresent notification: UNNotification, withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void) {
        
        completionHandler([.banner, .list, .badge, .sound])
        
    }
}

// MARK: Handle Local-Alert-Notification

extension NotificationService {
    func handleLocalNotification(_ response: UNNotificationResponse) {
        //let userInfo = response.notification.request.content.userInfo
        
        
        
    }
}
