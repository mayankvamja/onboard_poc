//
//  SettingsViewController.swift
//  POC
//
//  Created by Mayank Vamja on 29/04/21.
//

import UIKit

class SettingsViewController: UITableViewController {

    @IBOutlet var allowNotificationSwitch: UISwitch!
    @IBOutlet var usernameLabel: UILabel!
    @IBOutlet var useremailButton: UIButton!
    @IBOutlet var userPhoneButton: UIButton!
    
    var viewModel: SettingsViewModel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        viewModel = SettingsViewModel()

        setupUI()
        setupBinding()
    }
    
    func setupUI() {
        viewModel.checkNotificationAuthenticity()
        
        let user = viewModel.user
        let name = "\(user?.firstName ?? "-") \(user?.lastName ?? "-")"
        usernameLabel.text = name
        useremailButton.setTitle(user?.email, for: .normal)
        
        if let number = user?.phoneNumber {
            userPhoneButton.setTitle(number, for: .normal)
        } else {
            userPhoneButton.isHidden = true
        }
    }
    
    func setupBinding() {
        viewModel.setNotificationAuthorization = { allowed in
            DispatchQueue.main.async {
                self.allowNotificationSwitch.isOn = allowed
            }
        }
    }
    
    @IBAction func notificationSwitchChanged(_ sender: UISwitch) {
        sender.isOn = !sender.isOn
        UIApplication.shared.open(URL(string: UIApplication.openSettingsURLString)!, options: [:])
    }
    
    @IBAction func useremailTapped() {}
    @IBAction func userphoneTapped() {}
    
    @IBAction func onLogoutTapped() {
        if AuthDataStore.shared.logout() {
            self.view.makeToast("Logout".localized)
            
            let window = (UIApplication.shared.connectedScenes.first as! UIWindowScene).windows.first!
            
            let main = UIStoryboard(name: "Authentication", bundle: nil)
            let mainVC = main.instantiateInitialViewController()!
            window.rootViewController = mainVC
            
        }
    }
    
}
