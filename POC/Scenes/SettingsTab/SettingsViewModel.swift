//
//  SettingsViewModel.swift
//  POC
//
//  Created by Mayank Vamja on 29/04/21.
//

import Foundation
import UserNotifications
import UIKit

class SettingsViewModel {
    
    var user: AuthUser?
    var setNotificationAuthorization: ((_ allowed: Bool) -> Void)?
    var observer: AnyObject!
    
    init() {
        observer = NotificationCenter.default.addObserver(forName: UIApplication.didBecomeActiveNotification, object: nil, queue: nil) { [weak self] _ in self?.checkNotificationAuthenticity() }
        user = AuthDataStore.shared.loggedInUser()
    }
    
    func checkNotificationAuthenticity() {
        NotificationService.authorizationStatus { (status) in
            self.setNotificationAuthorization?(status == .authorized)
        }
    }
    
    deinit {
        NotificationCenter.default.removeObserver(observer as Any)
    }
    
}
