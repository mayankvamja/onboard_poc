//
//  DownloadSettingsModel.swift
//  POC
//
//  Created by Mayank Vamja on 28/04/21.
//

import Foundation

struct DownloadSettingsModel {
    
    private struct StorageKeys {
        static let HasSaved = "HAS_SETTINGS_SAVED"
        static let MaxDownloads = "FILE_MAX_DOWNLOADS"
        static let AllowBgDownloads = "ALLOW_BACKGROUND_DOWNLOADS"
        static let AutoPauseDownloads = "AUTO_PAUSE_DOWNLOADS"
    }
    
    private let storage = UserDefaults.standard
    
    var maxDownloads: Int {
        get { storage.integer(forKey: StorageKeys.MaxDownloads) }
        set { storage.set(newValue, forKey: StorageKeys.MaxDownloads) }
    }
    
    var allowBackground: Bool {
        get { storage.bool(forKey: StorageKeys.AllowBgDownloads) }
        set { storage.set(newValue, forKey: StorageKeys.AllowBgDownloads) }
    }
    
    var autoPause: Bool {
        get { storage.bool(forKey: StorageKeys.AutoPauseDownloads) }
        set { storage.set(newValue, forKey: StorageKeys.AutoPauseDownloads) }
    }
    
    init() {
        if storage.bool(forKey: StorageKeys.HasSaved) { return }
        
        maxDownloads = 3
        autoPause = true
        allowBackground = false
        storage.set(true, forKey: StorageKeys.HasSaved)
    }
    
}
