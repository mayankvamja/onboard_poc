//
//  DownloadListViewController.swift
//  POC
//
//  Created by Mayank Vamja on 12/04/21.
//

import UIKit

class DownloadListViewController: UITableViewController {
    
    private var viewModel: DownloadsViewModel!
    private var items: [DownloadFileItem] = []
    
    @IBOutlet var toolBarsWrapperView: UIView!
    @IBOutlet var listStypeSegmentControl: UISegmentedControl!
    
    @IBOutlet var startAllButton: UIBarButtonItem!
    @IBOutlet var pauseAllButton: UIBarButtonItem!
    @IBOutlet var cancelAllButton: UIBarButtonItem!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        viewModel = DownloadsViewModel()
        viewModel.delegate = self
        viewModel.fetchPhotos()
    }
    
    func reloadTable() {
        DispatchQueue.main.async { self.tableView.reloadData() }
    }
    
    func reloadTopBarButtons() {
        let startAll = items.contains { !$0.downloaded }
        let pauseAll = items.contains { $0.download?.status == .Downloading }
        let resumeAll = items.contains { $0.download != nil && $0.download?.status == .Paused }
        let cancelAll = items.contains { $0.download != nil }
        
        DispatchQueue.main.async {
            self.startAllButton.isEnabled = startAll
            self.pauseAllButton.isEnabled = pauseAll || resumeAll
            self.cancelAllButton.isEnabled = cancelAll
            self.pauseAllButton.title = resumeAll ? "Resume All" : "Pause All"
        }
        
    }
    
    func viewFile(from filepath: String) {
        
        let mime = FileMimeType(mimeType: FileMimeType.mimeTypeFor(url: viewModel.localFilePath(with: filepath)))
        
        if mime.isImage {
            
            let vc = self.storyboard?.instantiateViewController(identifier: "PhotoPageViewController") as! PhotoPageViewController
            vc.viewModel = PhotoViewModel(for: viewModel.documentsPath, start: filepath)
            vc.modalPresentationStyle = .fullScreen
            
            self.present(vc, animated: true)
        } else {
            
            let fileViewController = UIDocumentInteractionController(url: viewModel.localFilePath(with: filepath))
            fileViewController.delegate = self
            
            if fileViewController.presentPreview(animated: true) {
                DispatchQueue.main.async {
                    self.view.makeToast("Opening file")
                }
            } else {
                DispatchQueue.main.async {
                    self.view.makeToast("Unsupported file type.")
                }
            }
        }
        
    }
    
    @IBAction func startAllTapped(_ sender: UIBarButtonItem) {
        viewModel.startDownloadAll()
        checkListUpdate()
        reloadTable()
    }
    
    @IBAction func pauseResumeTapped(_ sender: UIBarButtonItem) {
        if sender.title == "Resume All" {
            viewModel.resumeDownloadAll()
        } else {
            viewModel.pauseDonwloadAll()
        }
        checkListUpdate()
        reloadTable()
    }
    
    @IBAction func cancelAllTapped(_ sender: UIBarButtonItem) {
        viewModel.cancelDownloadAll()
        checkListUpdate()
        reloadTable()
    }
    
    @IBAction func listTypeChanged(_ sender: UISegmentedControl) {
        checkListUpdate()
        reloadTable()
    }
    
    func checkListUpdate() {
        let index = listStypeSegmentControl.selectedSegmentIndex
        self.items = viewModel.filterList(index)
        reloadTopBarButtons()
    }
    
    @IBAction func onAppItemTapped(_ sender: UIBarButtonItem) {
        let vc = UIAlertController(title: "Add new", message: nil, preferredStyle: .alert)
        vc.view.tintColor = UIColor(named: "AppColor")!
        vc.addTextField { (textField) in
            textField.placeholder = "Name"
        }
        vc.addTextField { (textField) in
            textField.placeholder = "Download URL"
        }
        vc.addAction(UIAlertAction(title: "Add", style: .default, handler: { (_) in
            self.viewModel.check(newItemWith: vc.textFields![0].text!, urlString: vc.textFields![1].text!)
        }))
        vc.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
        
        present(vc, animated: true, completion: nil)
    }
}


// MARK: - TableView datSource & delegate

extension DownloadListViewController {
    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return items.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let item = items[indexPath.row]
        let cell = tableView.dequeueReusableCell(withIdentifier: "album_photo_cell", for: indexPath) as! DownloadListCell
        
        cell.delegate = self
        cell.config(from: item)
        return cell
    }
    
    override func tableView(_ tableView: UITableView, willSelectRowAt indexPath: IndexPath) -> IndexPath? {
        let canSelect = items[indexPath.row].downloaded
        guard canSelect else { return nil }
        return indexPath
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        tableView.deselectRow(at: indexPath, animated: true)
        
        let item = items[indexPath.row]
        guard let path = item.downloadedFilePath else { return }
        
        self.viewFile(from: path)
    }
    
    override func tableView(_ tableView: UITableView, contextMenuConfigurationForRowAt indexPath: IndexPath, point: CGPoint) -> UIContextMenuConfiguration? {
        
        let item = items[indexPath.row]
        guard item.downloaded else { return nil }
        
        return UIContextMenuConfiguration(identifier: indexPath as NSIndexPath, previewProvider: nil) { (_) -> UIMenu? in
            
            return UIMenu(title: "", children: [
                UIAction(title: "Remove file".localized, image: UIImage(systemName: "trash"), handler: { (_) in
                    self.viewModel.removeFile(item, at: indexPath.row)
                })
            ])
            
        }
        
    }
}

// MARK: DownloadsViewModelDelegate

extension DownloadListViewController: DownloadsViewModelDelegate {
    func loader(_ loading: Bool) {
        DispatchQueue.main.async {
            loading
                ? self.parent?.view.makeToastActivity(.center)
                : self.parent?.view.hideToastActivity()
        }
    }
    
    func didFilesLoad() {
        self.items = self.viewModel.data
        self.reloadTable()
    }
    
    func didFilesFailed(errorMessage: String) {
        print("-------")
        print("didFilesFailed with : ", errorMessage)
        print("-------")
        DispatchQueue.main.async { self.parent?.view.makeToast(errorMessage, style: .secondary) }
    }
    
    func updateRow(for fileId: String) {
        
        guard let index = self.items.firstIndex(where: {
            $0.id == fileId
        }) else { return }
        
        let rows = [IndexPath(row: index, section: 0)]
        DispatchQueue.main.async {
            self.tableView.reloadRows(at: rows, with: .none)
            self.checkListUpdate()
        }
        
    }
    
    func updateRow(for fileId: String, with data: RowDownloadData) {
        
        guard let index = self.items.firstIndex(where: {
            $0.id == fileId
        }) else { return }
        
        let row = IndexPath(row: index, section: 0)
        DispatchQueue.main.async {
            if let cell = self.tableView.cellForRow(at: row) as? DownloadListCell {
                cell.update(progress: data.progress, downloaded: data.downloadedSize, totalSize: data.totalSize)
            }
        }
        
    }
    
    
}


// MARK: DownloadListCellDelegate

extension DownloadListViewController: DownloadListCellDelegate {
    func cancelTapped(_ cell: DownloadListCell) {
        viewModel.cancelDownload(for: getItem(cell))
    }
    
    func downloadTapped(_ cell: DownloadListCell) {
        viewModel.startDownload(for: getItem(cell))
    }
    
    func pauseTapped(_ cell: DownloadListCell) {
        viewModel.pauseDownload(for: getItem(cell))
    }
    
    func resumeTapped(_ cell: DownloadListCell) {
        viewModel.resumeDownload(for: getItem(cell))
    }
    
    /// Helper function to get `DownloadFileItem` from `DownloadListCell`
    func getItem(_ cell: DownloadListCell) -> DownloadFileItem? {
        if let indexPath = tableView.indexPath(for: cell) {
            return items[indexPath.row]
        }
        return nil
    }
}


// MARK: UIDocumentInteractionControllerDelegate

extension DownloadListViewController: UIDocumentInteractionControllerDelegate {
    func documentInteractionControllerViewControllerForPreview(_ controller: UIDocumentInteractionController) -> UIViewController {
        return self
    }
}
