//
//  AlbumPhotoCell.swift
//  POC
//
//  Created by Mayank Vamja on 13/04/21.
//

import UIKit

protocol DownloadListCellDelegate: class {
    func cancelTapped(_ cell: DownloadListCell)
    func downloadTapped(_ cell: DownloadListCell)
    func pauseTapped(_ cell: DownloadListCell)
    func resumeTapped(_ cell: DownloadListCell)
}

class DownloadListCell: UITableViewCell {
    
    @IBOutlet var fileIconImageView: UIImageView!
    @IBOutlet var fileNameLabel: UILabel!
    @IBOutlet var filePathLabel: UILabel!
    @IBOutlet var downloadControls: UIStackView!
    
    @IBOutlet var downloadProgressBar: UIProgressView!
    @IBOutlet var progressLabel: UILabel!
    
    @IBOutlet var downloadButton: UIButton!
    @IBOutlet var cancelButton: UIButton!
    @IBOutlet var pauseButton: UIButton!
    @IBOutlet var resumeButton: UIButton!
    
    weak var delegate: DownloadListCellDelegate?
    
    @IBAction func downloadDidTap() {
        delegate?.downloadTapped(self)
    }
    
    @IBAction func cancelDidTap() {
        delegate?.cancelTapped(self)
    }
    
    @IBAction func pauseDidTap() {
        delegate?.pauseTapped(self)
    }
    
    @IBAction func resumeDidTap() {
        delegate?.resumeTapped(self)
    }
    
    let documentsPath = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first!
    
    func config(from fileItem: DownloadFileItem) {
        
        fileNameLabel.text = fileItem.name
        filePathLabel.text = "Doccuments/" + (fileItem.downloadedFilePath ?? "-")
        downloadProgressBar.progress = fileItem.download?.progress ?? 0.0
        updateUI(fileItem.download?.status ?? .None, downloaded: fileItem.downloaded)
    }
    
    func update(progress: Float, downloaded: String, totalSize : String) {
        downloadProgressBar.setProgress(progress, animated: true)
        progressLabel.text = "\(downloaded) / \(totalSize)"
    }
    
    // private methods
    
    private func updateUI(_ status: FileDownloadModel.Status, downloaded: Bool) {

        fileIconImageView.tintColor = downloaded ? .blue : .gray

        if downloaded {
            filePathLabel.isHidden = false
            downloadControls.isHidden = true
            downloadProgressBar.isHidden = true
            
        } else {
            
            filePathLabel.isHidden = true
            downloadControls.isHidden = false

            switch status {
            case .None:
                progressLabel.text = ""
                downloadProgressBar.isHidden = true
                buttonsHidden(false, true, true, true)
                
            case .Downloading:
                progressLabel.text = "Downloading".localized
                downloadProgressBar.isHidden = false
                buttonsHidden(true, false, true, false)
                
            case .Paused:
                progressLabel.text = "Paused".localized
                downloadProgressBar.isHidden = false
                buttonsHidden(true, true, false, false)
                
            case .Waiting:
                progressLabel.text = "Waiting".localized
                downloadProgressBar.isHidden = true
                buttonsHidden(true, true, true, false)
                
            default:
                progressLabel.text = ""
                downloadProgressBar.isHidden = true
                buttonsHidden(true, true, true, true)
            }
        }
    }
    
    private func buttonsHidden(_ download: Bool, _ pause: Bool, _ resume: Bool, _ cancel: Bool) {
        downloadButton.isHidden = download
        pauseButton.isHidden = pause
        resumeButton.isHidden = resume
        cancelButton.isHidden = cancel
    }
}
