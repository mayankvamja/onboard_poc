//
//  DownloadSettingViewController.swift
//  POC
//
//  Created by Mayank Vamja on 28/04/21.
//

import UIKit

class DownloadSettingViewController: UIViewController {

    @IBOutlet var autoPauseDownloadSwtich: UISwitch!
    @IBOutlet var allowBackgroundDownloadSwitch: UISwitch!
    @IBOutlet var maxDownloadStepper: UIStepper!
    @IBOutlet var maxDownloadsLabel: UILabel!
    
    var settings = DownloadSettingsModel()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        initializeData()
    }
    
    func initializeData() {
        maxDownloadStepper.value = Double(settings.maxDownloads)
        maxDownloadsLabel.text = "\(settings.maxDownloads)"
        autoPauseDownloadSwtich.isOn = settings.autoPause
        allowBackgroundDownloadSwitch.isOn = settings.allowBackground
    }
    
    @IBAction func maxDownloadsChanged(_ sender: UIStepper) {
        maxDownloadsLabel.text = "\(Int(sender.value))"
        settings.maxDownloads = Int(sender.value)
    }
    
    @IBAction func allowBackgroundDownloadsChanged(_ sender: UISwitch) {
        settings.allowBackground = sender.isOn
    }
    
    @IBAction func autoPauseDownloadsChanged(_ sender: UISwitch) {
        settings.autoPause = sender.isOn
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.dismiss(animated: true)
    }

}
