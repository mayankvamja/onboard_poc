//
//  PhotoPageViewController.swift
//  POC
//
//  Created by Mayank Vamja on 28/04/21.
//

import UIKit

class PhotoPageViewController: UIPageViewController {
    
    var viewModel: PhotoViewModel?
    var pages = [PhotoPageController]()
    var initialTouchPoint: CGPoint = CGPoint(x: 0,y: 0)

    @IBOutlet var panGestureRecognizer: UIPanGestureRecognizer!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.view.addGestureRecognizer(panGestureRecognizer)

        dataSource = self
        delegate = self
        
        viewModel?.delegate = self
        viewModel?.loadPhotos()
    }
    
    // Swipe to dismiss UIViewController
    @IBAction func panGestureRecognizerHandler(_ sender: UIPanGestureRecognizer) {
        let touchPoint = sender.location(in: self.view?.window)
        
        switch sender.state {
        case .began:
            initialTouchPoint = touchPoint

        case .changed:
            if touchPoint.y - initialTouchPoint.y > 0 {
                self.view.frame = CGRect(x: 0, y: touchPoint.y - initialTouchPoint.y, width: self.view.frame.size.width, height: self.view.frame.size.height)
            }
            
        case .ended, .cancelled:
            if touchPoint.y - initialTouchPoint.y > 100 {
                self.dismiss(animated: true, completion: nil)
            } else {
                UIView.animate(withDuration: 0.3, animations: {
                    self.view.frame = CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: self.view.frame.size.height)
                })
            }
            
        default:
            break
        }

    }

}

extension PhotoPageViewController: PhotoViewModelDelegate {
    func images(didLoad images: [UIImage], starting index: Int) {
        DispatchQueue.main.async {
            images.forEach { (image) in
                let vc = PhotoPageController()
                vc.setDisplayImage(image: image)
                vc.view.backgroundColor = UIColor.systemBackground.withAlphaComponent(0.2)
                
                self.pages.append(vc)
            }

            if self.pages.count > index {
                self.setViewControllers([self.pages[index]], direction: .forward, animated: true, completion: nil)
            } else if self.pages.count > 0 {
                self.setViewControllers([self.pages[0]], direction: .forward, animated: true, completion: nil)
            } else {
                self.dismiss(animated: true)
            }
        }
    }
    
    func images(didFailed errorMessage: String) {
        DispatchQueue.main.async {
            self.view.makeToast(errorMessage)
        }
    }
    
}

extension PhotoPageViewController: UIPageViewControllerDelegate, UIPageViewControllerDataSource {
        
    func pageViewController(_: UIPageViewController, viewControllerBefore viewController: UIViewController) -> UIViewController? {
        
        guard let viewControllerIndex = pages.firstIndex(of: viewController as! PhotoPageController) else { return nil }
                
        let previousIndex = viewControllerIndex - 1
        
        guard previousIndex >= 0 else { return pages.last }
        guard pages.count > previousIndex else { return nil }
        
        return pages[previousIndex]
    }
    
    func pageViewController(_: UIPageViewController, viewControllerAfter viewController: UIViewController) -> UIViewController? {
        
        guard let viewControllerIndex = pages.firstIndex(of: viewController as! PhotoPageController) else { return nil }
        
        let nextIndex = viewControllerIndex + 1
        
        guard pages.count != nextIndex else { return pages.first }
        guard pages.count > nextIndex else { return nil }
        
        return pages[nextIndex]
    }
    
    func presentationCount(for _: UIPageViewController) -> Int {
        return pages.count
    }
}
