//
//  PhotoPageController.swift
//  POC
//
//  Created by Mayank Vamja on 29/04/21.
//

import UIKit

class PhotoPageController: UIPageViewController {

    private let scrollView: ImageScrollView = {
        let sc = ImageScrollView(frame: .zero)
        sc.backgroundColor = UIColor.black.withAlphaComponent(0.3)
        return sc
    }()

    override func viewDidLoad() {
        super.viewDidLoad()
        setupUI()
    }
   
    func setupUI() {
        let w = self.view.frame.size.width
        let h = self.view.frame.size.height
        scrollView.frame = CGRect(x: 0, y: 0, width: w, height: h)
        
        self.view.addSubview(scrollView)
    }
    
    func setDisplayImage(image: UIImage) {
        self.scrollView.display(image: image)
    }

}
