//
//  PhotoViewController.swift
//  POC
//
//  Created by Mayank Vamja on 26/04/21.
//

import UIKit

/* Using InfiniteScrollView
class PhotoViewController: UIViewController {
        
    private let scrollView: IFScrollView = {
        let sc = IFScrollView(frame: .zero)
        sc.backgroundColor = .black
        return sc
    }()
    
    @IBOutlet var titleBar: UIView!
    @IBOutlet var titleLabel: UILabel!
    
    var viewModel: PhotoViewModel?
    var photos: [UIImage] = []
 var initialTouchPoint: CGPoint = CGPoint(x: 0,y: 0)

    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setUpUI()

        setupBinding()
        self.view.makeToastActivity(.center)
        viewModel?.loadPhotos()
        
    }
    
    private func setUpUI() {
        scrollView.frame = CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: self.view.frame.size.height)
        scrollView.pageDelegate = self
        
        self.view.addSubview(scrollView)
        self.view.bringSubviewToFront(titleBar)
        titleLabel.textColor = .white
    }
    
    func setupBinding() {
        
        viewModel?.imagesChanged = { (photos, startingIndex) in
            
            self.photos = photos
            DispatchQueue.main.async {

                self.scrollView.reloadScrollView()
                self.scrollView.scrollToPage(page: startingIndex, animated: false)
                self.view.hideToastActivity()

            }
            
        }
        
        viewModel?.imagesFailed = { message in
            self.view.makeToast(message, style: .error)
        }
                
    }
    
    // Swipe to dismiss UIViewController
    @IBAction func panGestureRecognizerHandler(_ sender: UIPanGestureRecognizer) {
        let touchPoint = sender.location(in: self.view?.window)
        
        switch sender.state {
        case .began:
            initialTouchPoint = touchPoint

        case .changed:
            if touchPoint.y - initialTouchPoint.y > 0 {
                self.view.frame = CGRect(x: 0, y: touchPoint.y - initialTouchPoint.y, width: self.view.frame.size.width, height: self.view.frame.size.height)
            }
            
        case .ended, .cancelled:
            if touchPoint.y - initialTouchPoint.y > 100 {
                self.dismiss(animated: true, completion: nil)
            } else {
                UIView.animate(withDuration: 0.3, animations: {
                    self.view.frame = CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: self.view.frame.size.height)
                })
            }
            
        default:
            break
        }

    }

}

extension PhotoViewController: IFScrollViewDelegate {
    func numberOfItems() -> Int {
        return photos.count
    }
    
    func scrollView(viewForItemAt index: Int) -> UIView {
        return ImageScrollView(frame: .zero, image: photos[index])
    }
    
    func scrollView(scrollView: UIScrollView, didChangePage page: Int) {
        
        if let view = scrollView.subviews[page - 1] as? ImageScrollView {
            view.asyncInit()
        }
        
        if let view = scrollView.subviews[page] as? ImageScrollView {
            view.asyncInit()
        }
        
        if let view = scrollView.subviews[page + 1] as? ImageScrollView {
            view.asyncInit()
        }
        
        self.titleLabel.text = "Photo (\(page) / \(photos.count))"
    }
    
}
*/
