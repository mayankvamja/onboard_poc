//
//  AlbumsDataSource.swift
//  POC
//
//  Created by Mayank Vamja on 12/04/21.
//

import Foundation
import CoreData
import UIKit

class CDDownloadsStorage {
    //static var storage = AlbumsDataStorage()
    
    let context: NSManagedObjectContext
    
    init() {
        context = {
            let container = NSPersistentContainer(name: "Downloads")
            container.loadPersistentStores(completionHandler: { (storeDescription, error) in
                if let error = error as NSError? {
                    debugPrint("Unresolved error \(error.localizedDescription), \(error.userInfo)")
                }
            })
            return container.viewContext
        }()
    }
    
    func save(completion: ((Bool) -> Void)? = nil) {
        guard context.hasChanges else {
            completion?(false)
            return
        }
        
        context.mergePolicy = NSMergePolicy(merge: .overwriteMergePolicyType)
        do {
            try context.save()
            completion?(true)
        } catch {
            completion?(false)
            debugPrint("Error saving context: \(error.localizedDescription)")
        }
    }
    
    func clean(completion: ((Bool) -> Void)? = nil) {
        fetchItems { (items) in
            items?.forEach({ context.delete($0) })
            save(completion: completion)
        }
    }
    
    func fetchItems(
        predicate: NSPredicate? = nil,
        sorting: [NSSortDescriptor] = [NSSortDescriptor(key: "createdAt", ascending: false)],
        completion: ([CDFileItem]?) -> Void
    ) {
        
        let request: NSFetchRequest<CDFileItem> = CDFileItem.fetchRequest()
        request.sortDescriptors = sorting
        request.predicate = predicate
        
        completion(try? context.fetch(request))
    }
    
    func saveFilePath(id: String, path: String?) {
        
        let predicate = NSPredicate(format: "id MATCHES %@", id)
        fetchItems(predicate: predicate) { (fileItems) in
            if let item = fileItems?.first {
                item.filePath = path
                self.save()
            }
        }
        
    }
    
}
