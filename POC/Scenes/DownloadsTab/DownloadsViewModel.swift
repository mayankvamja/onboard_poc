//
//  AlbumPhotosViewModel.swift
//  POC
//
//  Created by Mayank Vamja on 12/04/21.
//

import Foundation
import UIKit
import MobileCoreServices

protocol DownloadsViewModelDelegate {
    typealias RowDownloadData = (progress: Float, downloadedSize: String, totalSize: String)
    
    func loader(_ loading: Bool)
    func didFilesLoad()
    func didFilesFailed(errorMessage: String)
    
    func updateRow(for fileId: String)
    func updateRow(for fileId: String, with data: RowDownloadData)
}

class DownloadsViewModel: NSObject {
    
    var delegate: DownloadsViewModelDelegate?
    var loading = false { didSet { delegate?.loader(loading) } }
    
    let http = HttpUtils()
    let storage = CDDownloadsStorage()
    let settings = DownloadSettingsModel()
    
    var downloadService: FileDownloadService!
    var data = [DownloadFileItem]()
    
    let documentsPath = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first!
    
    override init() {
        super.init()
        
        let session: URLSession = settings.allowBackground
            ? URLSession(configuration: .background(withIdentifier: "com.example.POC.backgroundDownloadSession"), delegate: self, delegateQueue: nil)
            : URLSession(configuration: .default, delegate: self, delegateQueue: nil)
        
        downloadService = FileDownloadService(session: session)
        downloadService.statusChanged = { self.delegate?.updateRow(for: $0) }
    }
    
    func viewDidDissppear() {
        if settings.autoPause { pauseDonwloadAll() }
    }
    
    func viewDidAppear() {
        if settings.autoPause { resumeDownloadAll() }
    }
    
    
    func localFilePath(with name: String) -> URL {
        documentsPath.appendingPathComponent(name)
    }
    
    func fetchPhotos() {
        loading = true
        storage.fetchItems { (items) in
            if let items = items, items.count > 0 {
                self.data = items.map { DownloadFileItem(from: $0) }
                self.loading = false
                self.delegate?.didFilesLoad()
            } else {
                fetchPhotosFromServer()
            }
        }
    }
    
    func fetchPhotosFromServer() {
        
        let url = Endpoints.photosCollection
        
        http.makeRequest(from: url) { (data, response, error) in
            if let error = error {
                self.handleError(error: error)
                return
            }
            
            if let response = response as? HTTPURLResponse, response.statusCode == 200, let data = data {
                try? self.parseJson(from: data)
                return
            }
            
            self.loading = false
            self.delegate?.didFilesFailed(errorMessage: "Unable to fetch photos from server".localized)
        }
    }
    
    func saveToCoreData(_ result: [DownloadFileItem]) {
        result.forEach { (file) in
            let item = CDFileItem(context: storage.context)
            item.id = file.id
            item.name = file.name
            item.url = file.url
            item.createdAt = Date()
        }
        
        storage.save { (_) in self.loading = false }
    }
        
    func parseJson(from data: Data) throws {
        let res = try JSONDecoder().decode(PicsumPhotosResponse.self, from: data)
        
        self.data = res.map({ DownloadFileItem(from: $0) })
        self.saveToCoreData(self.data)
        self.delegate?.didFilesLoad()
    }
    
    func handleError(error: Error) {
        self.delegate?.didFilesFailed(errorMessage: "Unable to fetch photos from server".localized)
        self.loading = false
    }
    
    func filterList(_ index: Int) -> [DownloadFileItem] {
        switch index {
        case 1:
            return data.filter {
                $0.download?.status == .Downloading ||
                    $0.download?.status == .Waiting
            }
            
        case 2:
            return data.filter { $0.download?.status == .Paused }
            
        case 3:
            return data.filter { $0.downloaded }
            
        default:
            return data
        }
    }
    
    //
    func check(newItemWith name: String, urlString: String) {
        guard name.count > 0 else {
            delegate?.didFilesFailed(errorMessage: "Please provide name")
            return
        }
        
        guard urlString.count > 0 else {
            delegate?.didFilesFailed(errorMessage: "Please type URL")
            return
        }
        
        guard let url = URL(string: urlString),
              UIApplication.shared.canOpenURL(url)
        else {
            delegate?.didFilesFailed(errorMessage: "Please type correct URL")
            return
        }
        
        let newItem = DownloadFileItem(id: "\(Date().timeIntervalSince1970)", name: name, url: urlString)
        data.insert(newItem, at: 0)
        saveToCoreData([newItem])

        delegate?.didFilesLoad()
    }
    
    // MARK: Download Options Handler Methods
    
    func cancelDownload(for file: DownloadFileItem?) {
        guard let file = file else { return }
        downloadService.cancelDownload(file)
    }
    
    func resumeDownload(for file: DownloadFileItem?) {
        guard let file = file else { return }
        downloadService.resumeDownload(file)
    }
    
    func pauseDownload(for file: DownloadFileItem?) {
        guard let file = file else { return }
        downloadService.pauseDownload(file)
    }
    
    func startDownload(for file: DownloadFileItem?) {
        guard let file = file else { return }
        downloadService.startDownload(file)
    }
    
    func startDownloadAll() {
        data.forEach {  downloadService.startDownload($0) }
    }
    
    func pauseDonwloadAll() {
        data.forEach { downloadService.pauseDownload($0) }
    }
    
    func resumeDownloadAll() {
        data.forEach { downloadService.resumeDownload($0) }
    }
    
    func cancelDownloadAll() {
        data.forEach { downloadService.cancelDownload($0) }
    }
    
    // MARK: Context Menu Action Handlers
    
    func removeFile(_ file: DownloadFileItem, at index: Int) {
        
        try? FileManager.default.removeItem(at: documentsPath.appendingPathComponent(file.downloadedFilePath!))
        storage.saveFilePath(id: file.id, path: nil)
        
        file.download = nil
        file.downloadedFilePath = nil
        
        delegate?.updateRow(for: file.id)
    }
    
}

extension DownloadsViewModel: URLSessionDelegate, URLSessionDownloadDelegate {
    
    /*func urlSession(_ session: URLSession, task: URLSessionTask, didCompleteWithError error: Error?) {
        /*guard let sourceURL = task.originalRequest?.url,
         let item = data.first(where: {
         $0.url == sourceURL.absoluteString
         })
         else { return }*/
        
        //item.download = nil
        //downloadService.activeDownloads -= 1
        //downloadService.checkDownloadQueue()
    }*/
    
    func urlSession(_ session: URLSession, downloadTask: URLSessionDownloadTask, didFinishDownloadingTo location: URL) {
        
        guard let sourceURL = downloadTask.originalRequest?.url,
              let item = data.first(where: {
                $0.url == sourceURL.absoluteString
              })
        else { return }
        
        var fileName: String!
        if let suggestedName = downloadTask.response?.suggestedFilename {
            fileName = suggestedName
        } else {
            let ext = FileMimeType.mimeTypeToExtension(downloadTask.response?.mimeType) ?? "unknown"
            fileName = item.name + "." + ext
        }
        
        item.download = nil
        downloadService.activeDownloads -= 1
        downloadService.checkDownloadQueue()
        
        let destinationURL = localFilePath(with: fileName)
        let fileManager = FileManager.default
        try? fileManager.removeItem(at: destinationURL)
        
        do {
            
            try fileManager.copyItem(at: location, to: destinationURL)
            try? fileManager.removeItem(at: location)
            
            item.downloadedFilePath = destinationURL.pathComponents.last!
            
            storage.saveFilePath(id: item.id, path: destinationURL.pathComponents.last!)
            
        } catch {
            print("Could not copy file to disk: \(error.localizedDescription)")
        }
        
        delegate?.updateRow(for: item.id)
    }
    
    func urlSession(_ session: URLSession, downloadTask: URLSessionDownloadTask, didWriteData bytesWritten: Int64, totalBytesWritten: Int64, totalBytesExpectedToWrite: Int64) {
                
        guard
            let url = downloadTask.originalRequest?.url,
            let item = data.first(where: { $0.url == url.absoluteString }),
            let download = item.download
        else { return }
        
        download.progress = Float(totalBytesWritten) / Float(totalBytesExpectedToWrite)
        let downloadedSize = ByteCountFormatter.string(fromByteCount: totalBytesWritten, countStyle: .file)
        let totalSize = (totalBytesExpectedToWrite == NSURLSessionTransferSizeUnknown) ? "-" : ByteCountFormatter.string(fromByteCount: totalBytesExpectedToWrite, countStyle: .file)
        
        self.delegate?.updateRow(for: item.id, with: (download.progress, downloadedSize, totalSize))
        
    }
}
