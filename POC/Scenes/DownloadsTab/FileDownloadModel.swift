//
//  AlbumPhotosModel.swift
//  POC
//
//  Created by Mayank Vamja on 12/04/21.
//

import Foundation

class FileDownloadModel {
    
    enum Status {
        case None
        case Waiting
        case Downloading
        case Paused
        
        // TODO: Check for usage
        case Retrying
        case Failed
    }
    
    var status: Status = .None
    var progress: Float = 0.0
    var resumeData: Data?
    var task: URLSessionDownloadTask?
}

class DownloadFileItem: Equatable {
    static func == (lhs: DownloadFileItem, rhs: DownloadFileItem) -> Bool {
        return (lhs.id == rhs.id &&
        lhs.download?.status == rhs.download?.status)
    }
        
    let id: String
    let name: String
    let url: String
    
    var download: FileDownloadModel?
    var downloadedFilePath: String?
    var downloaded: Bool { downloadedFilePath != nil }
    
    init(id: String, name: String, url: String) {
        self.id = id
        self.name = name
        self.url = url
    }
    
    init(from picsum: PicsumCDFileItem) {
        self.id = picsum.id
        self.name = "\(picsum.author)-\(picsum.id)"
        self.url = picsum.downloadURL
    }
    
    init(from item: CDFileItem) {
        self.id = item.id!
        self.name = item.name!
        self.url = item.url!
        self.downloadedFilePath = item.filePath
    }
}

// MARK: - Codable PicsumCDFileItem

struct PicsumCDFileItem: Codable {
    let id, author: String
    let width, height: Int
    let url, downloadURL: String

    enum CodingKeys: String, CodingKey {
        case id, author, width, height, url
        case downloadURL = "download_url"
    }
}

typealias PicsumPhotosResponse = [PicsumCDFileItem]
