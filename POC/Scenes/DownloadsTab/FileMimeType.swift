//
//  FileMimeType.swift
//  POC
//
//  Created by Mayank Vamja on 01/05/21.
//

import Foundation
import MobileCoreServices

struct FileMimeType {
    
    let mimeType: String
    
    /// Converting MIME Type to extension (e.g.: jpg)
    static func mimeTypeToExtension(_ mimeType: String?) -> String? {
        guard let mimeType = mimeType else { return nil }
        let uti = UTTypeCreatePreferredIdentifierForTag(kUTTagClassMIMEType, mimeType as CFString, nil)
        guard let fileUTI = uti?.takeRetainedValue(),
              let fileExtension = UTTypeCopyPreferredTagWithClass(fileUTI, kUTTagClassFilenameExtension) else { return nil }
        
        let extensionString = String(fileExtension.takeRetainedValue())
        return extensionString
    }
    
    /// Coverting File Path to MIME Type
    static func mimeTypeFor(path: String) -> String {
        return mimeTypeFor(url: URL(fileURLWithPath: path))
    }
    
    static func mimeTypeFor(url: URL) -> String {
        let pathExtension = url.pathExtension
        
        if let uti = UTTypeCreatePreferredIdentifierForTag(kUTTagClassFilenameExtension, pathExtension as NSString, nil)?.takeRetainedValue() {
            if let mimetype = UTTypeCopyPreferredTagWithClass(uti, kUTTagClassMIMEType)?.takeRetainedValue() {
                return mimetype as String
            }
        }
        return "application/octet-stream"
    }
    
    var isImage: Bool {
        let mimeType = self.mimeType
        guard let uti = UTTypeCreatePreferredIdentifierForTag(kUTTagClassMIMEType, mimeType as CFString, nil)?.takeRetainedValue() else {
            return false
        }
        return UTTypeConformsTo(uti, kUTTypeImage)
    }
    var isAudio: Bool {
        let mimeType = self.mimeType
        guard let uti = UTTypeCreatePreferredIdentifierForTag(kUTTagClassMIMEType, mimeType as CFString, nil)?.takeRetainedValue() else {
            return false
        }
        return UTTypeConformsTo(uti, kUTTypeAudio)
    }
    var isVideo: Bool {
        let mimeType = self.mimeType
        guard  let uti = UTTypeCreatePreferredIdentifierForTag(kUTTagClassMIMEType, mimeType as CFString, nil)?.takeRetainedValue() else {
            return false
        }
        return UTTypeConformsTo(uti, kUTTypeMovie)
    }
}
