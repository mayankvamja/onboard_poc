//
//  PhotosDownloadService.swift
//  POC
//
//  Created by Mayank Vamja on 12/04/21.
//

import Foundation

class FileDownloadService {
    
    var activeDownloads: Int = 0
    var statusChanged: ((_ fileId: String) -> Void)?

    private let settings: DownloadSettingsModel
    private let downloadsQueue: DownloadQueue
    private let downloadsSession: URLSession

    init(session: URLSession) {
        settings = DownloadSettingsModel()
        downloadsSession = session
        downloadsQueue = DownloadQueue()
    }
    
    func cancelDownload(_ file: DownloadFileItem) {
        guard let download = file.download else { return }
        
        if download.status == .Downloading { activeDownloads -= 1 }
        download.task?.cancel()
        file.download = nil
        
        downloadsQueue.remove(file)
        checkDownloadQueue()
        statusChanged?(file.id)
    }
    
    func pauseDownload(_ file: DownloadFileItem) {
        guard let download = file.download,
              download.status == .Downloading
        else { return }
        
        download.task?.cancel(byProducingResumeData: { (data) in
            if data == nil {
                //print("Unable to produce resume data for : ", file.id)
            }
            download.resumeData = data
        })
        download.task?.cancel { download.resumeData = $0 }
        download.status = .Paused
        
        activeDownloads -= 1
        checkDownloadQueue()
        statusChanged?(file.id)
    }
    
    func resumeDownload(_ file: DownloadFileItem) {
        guard let download = file.download else { return }
        
        if let resumeData = download.resumeData {
            download.task = downloadsSession.downloadTask(withResumeData: resumeData)
        } else {
            download.task = downloadsSession.downloadTask(with: URL(string: file.url)!)
        }
                
        canStartDownload(file)
    }
    
    func startDownload(_ file: DownloadFileItem) {
        guard file.download == nil else { return }
        
        let download = FileDownloadModel()
        file.download = download
        
        download.task = downloadsSession.downloadTask(with: URL(string: file.url)!)
        
        canStartDownload(file)
    }
    
    private func canStartDownload(_ file: DownloadFileItem) {
        
        if (activeDownloads < settings.maxDownloads) {
            file.download!.task?.resume()
            file.download!.status = .Downloading
            activeDownloads += 1
        } else {
            file.download!.status = .Waiting
            downloadsQueue.enqueue(file)
        }
        
        statusChanged?(file.id)
    }
    
    func checkDownloadQueue() {
        if (activeDownloads < settings.maxDownloads && downloadsQueue.count > 0) {
            canStartDownload(downloadsQueue.dequeue()!)
        }
    }
}


class DownloadQueue {
    
    var data : [DownloadFileItem] = []
    var count: Int { return data.count }
    
    func dequeue() -> DownloadFileItem? {
        guard data.first != nil else { return nil }
        return data.remove(at: 0)
    }
    
    func enqueue(_ item: DownloadFileItem) {
        data.append(item)
    }
    
    func remove(_ item: DownloadFileItem) {
        if let index = data.firstIndex(where: { $0.id == item.id }) {
            data.remove(at: index)
        }
    }
    
    func clean() { data.removeAll() }
    
}
