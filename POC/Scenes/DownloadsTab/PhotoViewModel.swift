//
//  PhotoViewModel.swift
//  POC
//
//  Created by Mayank Vamja on 26/04/21.
//

import Foundation
import UIKit

protocol PhotoViewModelDelegate {
    func images(didLoad images: [UIImage], starting index: Int)
    func images(didFailed errorMessage: String)
}

class PhotoViewModel {
    
    var delegate: PhotoViewModelDelegate?
    var directory: URL
    var initialPhotoPath: String
    
    init(for dir: URL, start: String) {
        self.directory = dir
        self.initialPhotoPath = start
    }
    
    func loadPhotos() {
        
        DispatchQueue.global(qos: .userInteractive).async {
            
            let directoryContent = try! FileManager.default.contentsOfDirectory(at: self.directory, includingPropertiesForKeys: [.addedToDirectoryDateKey])
            
            var images: [UIImage] = []
            var startingIndex = 0

            for (index, imageURL) in directoryContent.enumerated() {
                if let image = UIImage(contentsOfFile: imageURL.path) {
                    images.append(image)

                    if imageURL.lastPathComponent == self.initialPhotoPath {
                        startingIndex = index
                    }
                }
            }
            
            if images.count > 0 {
                self.delegate?.images(didLoad: images, starting: startingIndex)
            } else {
                self.delegate?.images(didFailed: "Failed to load images".localized)
            }
        }
        
    }
    
}
