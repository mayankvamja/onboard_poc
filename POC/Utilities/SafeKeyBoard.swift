//
//  SafeKeyBoard.swift
//  POC
//
//  Created by Mayank Vamja on 29/04/21.
//

import UIKit

struct SafeKeyBoard {
    
    static func listeners(
        show: @escaping (UIEdgeInsets) -> Void,
        hide: @escaping (UIEdgeInsets) -> Void
    ) -> [AnyObject] {
        return [
            NotificationCenter.default.addObserver(forName: UIResponder.keyboardWillShowNotification, object: nil, queue: nil) { (note) in
                
                guard let keyboardFrame = (note.userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue else { return }
                
                UIView.animate(withDuration: CATransaction.animationDuration(), animations: {
                    show(UIEdgeInsets(top: 0.0, left: 0.0, bottom: keyboardFrame.size.height, right: 0.0))
                }, completion: nil)
                
            },
            NotificationCenter.default.addObserver(forName: UIResponder.keyboardWillHideNotification, object: nil, queue: nil) { (_) in
                
                UIView.animate(withDuration: CATransaction.animationDuration(), animations: {
                    hide(.zero)
                }, completion: nil)
                
            }
        ]
    }
    
    static func removeListeners(_ observers: [AnyObject]) {
        observers.forEach { NotificationCenter.default.removeObserver($0) }
    }
    
}
