//
//  InfiniteScrollView.swift
//  POC
//
//  Created by Mayank Vamja on 26/04/21.
//

/*
import UIKit

public protocol IFScrollViewDelegate: UIScrollViewDelegate {
    
    // Returns number of pages for infinite scrollview
    func numberOfItems() -> Int
    
    // Returns UIView for page at index
    func scrollView(viewForItemAt index: Int) -> UIView
    
    // Called when current page changes/scrolled
    func scrollView(scrollView: UIScrollView, didChangePage page: Int)
    
}

public class IFScrollView: UIScrollView {
    
    public enum IFScrollViewDirection {
        case horizontal
        case vertical
    }
    
    weak public var pageDelegate: IFScrollViewDelegate?
    public var direction: IFScrollViewDirection = .horizontal
    
    private var numberOfItems: Int {
        return pageDelegate?.numberOfItems() ?? 0
    }
    
    // MARK: Constructors
    
    override public init(frame: CGRect) {
        super.init(frame: frame)
        
        setup()
    }
    
    required public init?(coder: NSCoder) {
        super.init(coder: coder)
    }
    
    // MARK: private methods
    
    private func setup() {
        delegate = self
        
        translatesAutoresizingMaskIntoConstraints = false
        isPagingEnabled = true
        showsHorizontalScrollIndicator = false
        showsVerticalScrollIndicator = false
    }
    
    private func addViewToIndex(viewIndex: Int, index: Int) {
        
        guard let view = pageDelegate?.scrollView(viewForItemAt: viewIndex) else {
            print("No View for index: ", index)
            return
        }
        
        //let view = UIView(frame: .zero)
        view.backgroundColor = .red
        
        view.translatesAutoresizingMaskIntoConstraints = false
        
        if direction == .horizontal {
            view.frame = CGRect(x: CGFloat(index) * frame.size.width, y: 0, width: frame.size.width, height: frame.size.height)
        } else {
            view.frame = CGRect(x: 0, y: CGFloat(index) * frame.size.height, width: frame.size.width, height: frame.size.height)
        }
        
        addSubview(view)
        
    }
    
    // MARK: public methods
    
    /* Reload all pages */
    public func reloadScrollView() {
        guard self.numberOfItems > 0 else { return }
        
        
        if self.numberOfItems == 1 {
            addViewToIndex(viewIndex: 0, index: 0)
            isScrollEnabled = false
            return
        }
        
        addViewToIndex(viewIndex: 0, index: numberOfItems + 1)
        addViewToIndex(viewIndex: numberOfItems - 1, index: 0)
        
        for index in 0..<self.numberOfItems {
            addViewToIndex(viewIndex: index, index: index + 1)
        }
        
        contentSize = (direction == .horizontal) ? CGSize(width: CGFloat(numberOfItems+2) * frame.size.width, height: frame.size.height) : CGSize(width: frame.size.width, height: CGFloat(numberOfItems+2) * frame.size.height)
        
        contentOffset = (direction == .horizontal) ? CGPoint(x: frame.size.width, y: contentOffset.y) : CGPoint(x: contentOffset.x, y: frame.size.height)
        
        
        
        print("-----------")
        print("----PAGES ADDED------")
        print("-----------")
        
    }
    
    /* Scroll to page at index */
    public func scrollToPage(page: Int, animated: Bool) {
        
        var frame: CGRect = self.frame
        
        if (direction == .horizontal) {
            frame.origin.x = frame.size.width * CGFloat(page)
            frame.origin.y = 0
        } else {
            frame.origin.x = 0
            frame.origin.y = frame.size.height * CGFloat(page)
        }
        
        scrollRectToVisible(frame, animated: animated)
    }
    
}

extension IFScrollView: UIScrollViewDelegate {
    
    /* For Infinite scrolling */
    
    public func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        
        var currentPage:Int = (direction == .horizontal) ? Int(scrollView.contentOffset.x / scrollView.frame.size.width) : Int(scrollView.contentOffset.y / scrollView.frame.size.height)
        
        let numberOfItems = self.pageDelegate?.numberOfItems() ?? 0
        
        switch currentPage {
        case 0:
            currentPage = 1
            contentOffset = (direction == .horizontal) ? CGPoint(x: scrollView.frame.size.width * CGFloat(numberOfItems), y: scrollView.contentOffset.y) : CGPoint(x: scrollView.contentOffset.x, y: scrollView.frame.size.height * CGFloat(numberOfItems))
            
        case numberOfItems:
            currentPage = 1
            contentOffset = (direction == .horizontal) ? CGPoint(x: 0, y: scrollView.contentOffset.y) : CGPoint(x: scrollView.contentOffset.y, y: 0)
            
        case numberOfItems + 1:
            currentPage = 1
            
        default:
            currentPage = currentPage + 1
        }
        
        pageDelegate?.scrollView(scrollView: self, didChangePage: currentPage)
        pageDelegate?.scrollViewWillBeginDecelerating?(scrollView)
        
    }
    
    /* Other UIScrollViewDelegate Methods */
    
    public func scrollViewDidScroll(_ scrollView: UIScrollView) {
        pageDelegate?.scrollViewDidScroll?(scrollView)
    }
    
    public func scrollViewWillBeginDragging(_ scrollView: UIScrollView) {
        pageDelegate?.scrollViewWillBeginDragging?(scrollView)
    }
    
    public func scrollViewWillEndDragging(_ scrollView: UIScrollView, withVelocity velocity: CGPoint, targetContentOffset: UnsafeMutablePointer<CGPoint>) {
        pageDelegate?.scrollViewWillEndDragging?(scrollView, withVelocity: velocity, targetContentOffset: targetContentOffset)
    }
    
    public func scrollViewDidEndDragging(_ scrollView: UIScrollView, willDecelerate decelerate: Bool) {
        pageDelegate?.scrollViewDidEndDragging?(scrollView, willDecelerate: decelerate)
    }
    
    public func scrollViewWillBeginDecelerating(_ scrollView: UIScrollView) {
        pageDelegate?.scrollViewDidEndDecelerating?(scrollView)
    }
    
    public func scrollViewDidEndScrollingAnimation(_ scrollView: UIScrollView) {
        pageDelegate?.scrollViewDidEndScrollingAnimation?(scrollView)
    }
    
    public func scrollViewWillBeginZooming(_ scrollView: UIScrollView, with view: UIView?) {
        pageDelegate?.scrollViewWillBeginZooming?(scrollView, with: view)
    }
    
    public func scrollViewDidEndZooming(_ scrollView: UIScrollView, with view: UIView?, atScale scale: CGFloat) {
        pageDelegate?.scrollViewDidEndZooming?(scrollView, with: view, atScale: scale)
    }
    
    public func scrollViewShouldScrollToTop(_ scrollView: UIScrollView) -> Bool {
        guard let method = pageDelegate?.scrollViewShouldScrollToTop else {
            return false
        }
        
        return method(scrollView)
    }
    
    @available(iOS 11.0, *)
    public func scrollViewDidChangeAdjustedContentInset(_ scrollView: UIScrollView) {
        pageDelegate?.scrollViewDidChangeAdjustedContentInset?(scrollView)
    }
    
    public func viewForZooming(in scrollView: UIScrollView) -> UIView? {
        guard let method = pageDelegate?.viewForZooming else {
            return nil
        }
        return method(scrollView)
    }
    
    public func scrollViewDidZoom(_ scrollView: UIScrollView) {
        pageDelegate?.scrollViewDidZoom?(scrollView)
    }
    
}

 */
