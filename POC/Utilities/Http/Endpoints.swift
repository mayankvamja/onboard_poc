//
//  Endpoints.swift
//  POC
//
//  Created by Mayank Vamja on 12/04/21.
//

import Foundation

struct Endpoints {
    static let appId = "60727dacc5ca23c4b0e466b0"
    static let baseUrl = "https://dummyapi.io/data/api"
    static let posts = "https://dummyapi.io/data/api/post"
    
    static let photosCollection = "https://picsum.photos/v2/list?page=1&limit=100"
}
