//
//  AppIdInterceptor.swift
//  POC
//
//  Created by Mayank Vamja on 12/04/21.
//

import Foundation


class AppIdInterceptor: URLProtocol {
    
    private static let appId: String = "60727dacc5ca23c4b0e466b0"
    
    override class func canInit(with request: URLRequest) -> Bool {
        return true
    }
    
    override class func canonicalRequest(for request: URLRequest) -> URLRequest {
        var newRequest = request
        newRequest.addValue(AppIdInterceptor.appId, forHTTPHeaderField: "app-id")
        return newRequest
    }
    
    override func startLoading() {
        self.task?.resume()
    }
    
    override func stopLoading() {
        self.task?.cancel()
    }
    
}
