//
//  HttpUtils.swift
//  POC
//
//  Created by Mayank Vamja on 12/04/21.
//

import Foundation

struct HttpUtils {
    var session: URLSession
    
    init(config: URLSessionConfiguration = URLSessionConfiguration.default) {
        
        session = URLSession(configuration: config)
    }
    
    func makeRequest(from url: String, parameters: [String: String]? = nil, method: String = "get", body: Data? = nil, completion: @escaping (Data?, URLResponse?, Error?) -> Void) {
        
        var request = URLRequest(getUrl(from: url, parameters: parameters))

        request.httpMethod = method
        request.httpBody = body
        request.setValue("application/json", forHTTPHeaderField: "Content-Type")
                
        self.startDataTask(request, completion: completion)

    }
    
    func startDataTask(_ request: URLRequest, completion: @escaping (Data?, URLResponse?, Error?) -> Void) {
        let task = session.dataTask(with: request, completionHandler: completion)
        task.resume()
    }
    
    func getUrl(from url: String, parameters: [String: String]?) -> URL {
        var components = URLComponents(string: url)!
        if let parameters = parameters {
            components.queryItems = parameters.map { (key, value) in
                URLQueryItem(name: key, value: value)
            }
        }
        return components.url!
    }
    
}

extension URLRequest {
    init(_ url: URL) {
        self.init(url: url)
        if url.absoluteString.contains(Endpoints.baseUrl) {
            self.setValue(Endpoints.appId, forHTTPHeaderField: "app-id")
        }
    }
}
