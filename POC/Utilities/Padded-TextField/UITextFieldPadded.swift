//
//  UITextFieldPadded.swift
//  POC
//
//  Created by Mayank Vamja on 29/04/21.
//

import UIKit

class UITextFieldPadded: UITextField {
    @IBInspectable var paddingVertical: CGFloat = 0.0
    @IBInspectable var paddingHorizontal: CGFloat = 0.0
    
    var padding: UIEdgeInsets {
        UIEdgeInsets(top: paddingVertical, left: paddingHorizontal, bottom: paddingVertical, right: paddingHorizontal)
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
    }
    
    override func textRect(forBounds bounds: CGRect) -> CGRect {
        bounds.inset(by: padding)
    }
    
    override func placeholderRect(forBounds bounds: CGRect) -> CGRect {
        bounds.inset(by: padding)
    }
    
    override func editingRect(forBounds bounds: CGRect) -> CGRect {
        bounds.inset(by: padding)
    }
}
