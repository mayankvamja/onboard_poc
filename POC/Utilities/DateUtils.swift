//
//  DateExtension.swift
//  POC
//
//  Created by Mayank Vamja on 24/04/21.
//

import Foundation

struct DateUtils {
    
    static func timeAgo(from date: Date) -> String {
        
        let seconds = Int(-date.timeIntervalSinceNow)
        guard seconds > 60 else { return "\(seconds) seconds ago" }
        
        let minutes = seconds / 60
        guard minutes > 60 else { return "\(minutes) mins ago" }
        
        let hours = minutes / 60
        guard hours > 24 else { return "\(hours) hours ago" }
        
        let days = hours / 24
        guard days > 7 else { return "\(days) days ago" }
        
        let weeks = days / 7
        guard weeks > 52 else { return "\(weeks) weeks ago" }
        
        let years = weeks / 52
        return "\(years) years ago"
        
    }

    static func timeAgo(from utc: String) -> String {
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSSZ"
        
        guard let date = formatter.date(from: utc) else { return "- -" }
        
        
        return timeAgo(from: date)
        
    }
    
    static func timeAfter(from date: Date) -> String {
        
        let seconds = Int(date.timeIntervalSinceNow)
        guard seconds > 60 else { return "\(seconds) seconds after" }
        
        let minutes = seconds / 60
        guard minutes > 60 else { return "\(minutes) mins after" }
        
        let hours = minutes / 60
        guard hours > 24 else { return "\(hours) hours after" }
        
        let days = hours / 24
        guard days > 7 else { return "\(days) days after" }
        
        let weeks = days / 7
        guard weeks > 52 else { return "\(weeks) weeks after" }
        
        let years = weeks / 52
        return "\(years) years after"
        
    }
}
