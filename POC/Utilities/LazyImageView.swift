//
//  LazyImageView.swift
//  POC
//
//  Created by Mayank Vamja on 12/04/21.
//

import Foundation
import UIKit

class LazyImageView: UIImageView {

    private let imageCache = NSCache<AnyObject, UIImage>()
    private var urlString: String?

    func loadImage(from imageUrlString: String, with placeholder: UIImage? = nil, completion: ((CGFloat, CGFloat) -> Void)? = nil) {
        
        guard let imageURL = URL(string: imageUrlString) else { return }
        
        urlString = imageUrlString
        image = placeholder

        if let cachedImage = self.imageCache.object(forKey: imageUrlString as AnyObject) {
            self.image = cachedImage
            completion?(cachedImage.size.width, cachedImage.size.height)
            return
        }

        DispatchQueue.global().async {
            [weak self] in
            if let imageData = try? Data(contentsOf: imageURL) {
                if let image = UIImage(data: imageData) {
                    self?.imageCache.setObject(image, forKey: imageUrlString as AnyObject)
                    if imageUrlString == self?.urlString {
                        DispatchQueue.main.async {
                            self?.image = image
                            completion?(image.size.width, image.size.height)
                        }
                    }
                }
            }
        }
    }
    
    func loadImage(from url: URL, with placeholder: UIImage? = nil) {
                
        urlString = url.absoluteString
        image = placeholder

        if let cachedImage = self.imageCache.object(forKey: url as AnyObject) {
            self.image = cachedImage
            return
        }

        DispatchQueue.global().async {
            [weak self] in

            if let imageData = try? Data(contentsOf: url) {
                
                //print("image loaded from url")
                
                if let image = UIImage(data: imageData) {
                    
                    self?.imageCache.setObject(image, forKey: url as AnyObject)
                    
                    if url.absoluteString == self?.urlString {
                        DispatchQueue.main.async {
                            self?.image = image
                        }
                    }
                    
                }
            }
        }
    }
}
