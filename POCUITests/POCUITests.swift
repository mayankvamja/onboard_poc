//
//  POCUITests.swift
//  POCUITests
//
//  Created by Mayank Vamja on 09/04/21.
//

import XCTest

class POCUITests: XCTestCase {
    
    let app = XCUIApplication()
    let testEmail = "email@example.com"
    
    override func setUpWithError() throws {
        continueAfterFailure = false
        app.launch()
    }
    
    override func tearDownWithError() throws {
        app.terminate()
    }
    
    func testExample() throws {
        let elementsQuery = app.scrollViews.otherElements
        let tablesQuery = app.tables
        
        if app.tabBars["Tab Bar"].buttons["Settings"].exists {
            app.tabBars["Tab Bar"].buttons["Settings"].tap()
            tablesQuery/*@START_MENU_TOKEN@*/.buttons["Logout"]/*[[".cells.buttons[\"Logout\"]",".buttons[\"Logout\"]"],[[[-1,1],[-1,0]]],[0]]@END_MENU_TOKEN@*/.tap()
        }
        
        let noAccount = elementsQuery/*@START_MENU_TOKEN@*/.staticTexts["Don't have an account? Register now"]/*[[".buttons[\"Don't have an account? Register now\"].staticTexts[\"Don't have an account? Register now\"]",".staticTexts[\"Don't have an account? Register now\"]"],[[[-1,1],[-1,0]]],[0]]@END_MENU_TOKEN@*/
        noAccount.tap()
        
        let firstNameTextField = elementsQuery.textFields["First name"]
        guard firstNameTextField.waitForExistence(timeout: 5) else {
            XCTFail()
            return
        }
        firstNameTextField.tap()
        firstNameTextField.typeText("Test")
        
        let lastNameTextField = elementsQuery.textFields["Last name"]
        lastNameTextField.tap()
        lastNameTextField.typeText("Test")
        
        let phoneNumberOptionalTextField = elementsQuery.textFields["Phone number (Optional)"]
        phoneNumberOptionalTextField.tap()
        phoneNumberOptionalTextField.typeText("999")
        
        let result = "Phone number is required"
        XCTAssertNotNil(elementsQuery.staticTexts[result])
        
        phoneNumberOptionalTextField.typeText("9999999999")
        
        let emailTextField = elementsQuery.textFields["Email"]
        emailTextField.tap()
        emailTextField.typeText(testEmail)
        
        let passwordSecureTextField = elementsQuery.secureTextFields["Password"]
        passwordSecureTextField.tap()
        passwordSecureTextField.typeText("Test@123")
        
        let confirmPasswordSecureTextField = elementsQuery.secureTextFields["Confirm Password"]
        confirmPasswordSecureTextField.tap()
        confirmPasswordSecureTextField.typeText("Test@123")
        
        elementsQuery.buttons["Submit"].tap()
        
        let loginButton = elementsQuery.buttons["Login"]
        guard loginButton.waitForExistence(timeout: 5) else {
            XCTFail()
            return
        }
        
        let emailTextFieldLogin = elementsQuery.textFields["Email"]
        emailTextFieldLogin.tap()
        emailTextFieldLogin.typeText(testEmail)
        
        let passwordSecureTextFieldLogin = elementsQuery.secureTextFields["Password"]
        passwordSecureTextFieldLogin.tap()
        passwordSecureTextFieldLogin.typeText("Test@123")
        
        loginButton.tap()
        
        let tabBar = app.tabBars["Tab Bar"]
        guard tabBar.waitForExistence(timeout: 5) else {
            XCTFail()
            return
        }
        
        //tabBar.buttons["Settings"].tap()
        //tablesQuery/*@START_MENU_TOKEN@*/.buttons["Logout"]/*[[".cells.buttons[\"Logout\"]",".buttons[\"Logout\"]"],[[[-1,1],[-1,0]]],[0]]@END_MENU_TOKEN@*/.tap()
        
        let likeButton = tablesQuery.buttons.containing(NSPredicate(format: "label CONTAINS 'likes'")).firstMatch
        if likeButton.waitForExistence(timeout: 20) {
            likeButton.tap()
            tablesQuery.cells.containing(.staticText, identifier:"Gratitude short-coated tan dog on seashore").buttons["Comments"].tap()
            app.navigationBars["POC.PostDetailView"].buttons["Feed"].tap()
        }
        
        tabBar.buttons["Downloads"].tap()
        
        if tablesQuery.cells.containing(.button, identifier: "Download").firstMatch.waitForExistence(timeout: 10) {
            tablesQuery.buttons["Completed"].firstMatch.tap()
        }
        
        app.tabBars["Tab Bar"].buttons["More"].tap()
        
        let permissionAlertAllow = app.alerts["“POC” Would Like to Send You Notifications"].scrollViews.otherElements.buttons["Allow"]
        if permissionAlertAllow.waitForExistence(timeout: 2) {
            permissionAlertAllow.tap()
        }
        
        let notificationTitleTextField = tablesQuery/*@START_MENU_TOKEN@*/.textFields["Notification title"]/*[[".cells.textFields[\"Notification title\"]",".textFields[\"Notification title\"]"],[[[-1,1],[-1,0]]],[0]]@END_MENU_TOKEN@*/
        notificationTitleTextField.tap()
        notificationTitleTextField.typeText("Notification UI Test")
        
        let scheduledNotifs = tablesQuery.staticTexts.containing(NSPredicate(format: "label CONTAINS 'Scheduled Notifications'"))
        let addNotificationButton = tablesQuery/*@START_MENU_TOKEN@*/.buttons["Add"]/*[[".cells.buttons[\"Add\"]",".buttons[\"Add\"]"],[[[-1,1],[-1,0]]],[0]]@END_MENU_TOKEN@*/
        while !scheduledNotifs.firstMatch.isHittable {
            app.swipeUp()
        }
        
        addNotificationButton.tap()
        
        tablesQuery.staticTexts.containing(NSPredicate(format: "label CONTAINS 'Notifications History'")).firstMatch.tap()
        app.navigationBars["Notifications History"].buttons["Utilities"].tap()
        
        scheduledNotifs.firstMatch.tap()
        if tablesQuery.buttons["Cancel All"].exists {
            tablesQuery.buttons["Cancel All"].tap()
        }
        app.navigationBars["Scheduled Notifications"].buttons["Utilities"].tap()
        
        app.tabBars["Tab Bar"].buttons["Settings"].tap()
        tablesQuery/*@START_MENU_TOKEN@*/.buttons["Logout"]/*[[".cells.buttons[\"Logout\"]",".buttons[\"Logout\"]"],[[[-1,1],[-1,0]]],[0]]@END_MENU_TOKEN@*/.tap()
        
        guard loginButton.waitForExistence(timeout: 2) else {
            XCTFail()
            return
        }
    }
}
